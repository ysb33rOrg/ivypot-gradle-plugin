Repositories are automatically added from anything declared int he standard `repositories` Gradle DSL block.
Only Ivy & Maven repositories are supported.
`mavenLocal` is always excluded.
If is also possible to exclude specific repositories.

.build.gradle
[source,groovy]
----
repositories {
  maven {
    name = 'myRepo'
    url = 'https://my-repo.example'
  }
  ivy {
    name = 'myCustomIvy'
    patternLayout {
     artifact '[module]/[revision]/[artifact](.[ext])'
     ivy '[module]/[revision]/ivy.xml'
    }
  }
}

ivypot {
  repositories {
    excludeByName 'myRepo' // <.>
    forIvy 'myCustomIvy', {
      ivyPatterns '[module]/[revision]/ivy.xml' // <.>
      artifactPatterns '[module]/[revision]/[artifact](.[ext])' // <.>
      artifactsOnly() // <.>
    }
  }
}
----
<.> Exclude one or more repositories by name.
<.> Declare one or more Ivy patterns to be used for the named repository.
<.> Declare one or more Ivy artifact patterns to be used for the named repository.
<.> Declare that the named Ivy repository contains no metadata.

NOTE: Due to a restriction in the Gradle API, we cannot retrieve Ivy information from the declared repositories.
It is therefore necessary to duplicate some information.