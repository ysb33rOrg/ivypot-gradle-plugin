/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2020
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//
package org.ysb33r.ivypot.remote

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.MapConstructor
import groovy.transform.ToString
import org.jetbrains.annotations.NotNull

/**
 * Describes an Ivy dependency.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
@MapConstructor(includeSuperFields = true, includeSuperProperties = true)
@ToString(includeNames = true, includeSuperFields = true, includeSuperProperties = true)
@EqualsAndHashCode
class IvyDependency extends BinaryDependency implements Serializable, Comparable<BinaryDependency> {
    boolean transitive
    String confFilter

    @Override
    int compareTo(@NotNull BinaryDependency o) {
        if (o instanceof IvyDependency) {
            final ret = super.compareTo(o)
            if (ret) {
                ret
            } else {
                final t = transitive <=> o.transitive
                final cf = confFilter <=> o.confFilter
                t ?: cf
            }
        } else {
            super.compareTo(o)
        }
    }
}
