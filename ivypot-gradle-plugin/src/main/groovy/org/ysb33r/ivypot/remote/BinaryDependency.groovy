/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2020
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//
package org.ysb33r.ivypot.remote

import groovy.transform.Canonical
import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.MapConstructor
import groovy.transform.ToString
import org.jetbrains.annotations.NotNull

/**
 * Metadata about a binary package.
 */
@CompileStatic
@MapConstructor
@Canonical
@EqualsAndHashCode
@ToString(includeNames = true)
class BinaryDependency implements Serializable, Comparable<BinaryDependency> {
    String organisation
    String module
    String revision
    String extension
    String classifier
    String typeFilter

    @Override
    int compareTo(@NotNull BinaryDependency o) {
        (organisation <=> o.organisation) ?:
            (module <=> o.module) ?:
                (revision <=> o.revision) ?:
                    (classifier <=> o.classifier) ?:
                        (typeFilter <=> o.typeFilter) ?:
                            (extension <=> o.extension)
    }
}
