/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.remote

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.apache.ivy.core.IvyPatternHelper
import org.ysb33r.ivypot.gradle.errors.BadBinaryException

import java.nio.file.Files

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE
import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING

/**
 * Performs the actual work of downloading binaries.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@Slf4j
class BinaryExecutor {
    public static final String BINARY_SUBDIR = 'binaries'

    static void runUpdater(
        File repoRoot,
        Map<String, BinaryRepository> binaryRepositories,
        Collection<BinaryDependency> binaries,
        boolean overwrite
    ) {
        List<BinaryDependency> deps = []
        deps.addAll(binaries)
        final downloader = new BinaryDownloader(repoRoot, binaryRepositories)
        downloader.resolve(deps.sort().unique(), overwrite)
    }

    private static class BinaryDownloader {

        BinaryDownloader(
            File repoRoot,
            Map<String, BinaryRepository> binaryRepositories
        ) {
            this.repoRoot = repoRoot
            repositories.putAll(binaryRepositories)
        }

        void resolve(
            Collection<BinaryDependency> binaries,
            boolean overwrite
        ) {
            repoRoot.mkdirs()
            List<Artifact> artifacts = downloadableArtifacts(binaries)

            artifacts.each {
                if (overwrite || !it.destinationPath.exists()) {
                    log.info("Retrieving ${it.downloadUri}")
                    switch (it.downloadUri.scheme) {
                        case 'file':
                            copyFile(new File(it.downloadUri), it.destinationPath)
                            break
                        case 'http':
                        case 'https':
                            downloadFile(it.downloadUri, it.destinationPath)
                            break
                        default:
                            throw new BadBinaryException("${it.downloadUri} is not a supported scheme")
                    }
                }
            }
        }

        List<Artifact> downloadableArtifacts(Collection<BinaryDependency> binaries) {
            binaries.collect {
                BinaryRepository repo = repositories[it.organisation]

                if (repo == null) {
                    throw new BadBinaryException("Organisation '${it.organisation}' is not mapped to a repository")
                }

                String relativePath = makeRelativePath(repo.artifactPattern, it)

                new Artifact(
                    downloadUri: repo.rootUri.resolve(relativePath),
                    destinationPath: new File(repoRoot, "${BINARY_SUBDIR}/${it.organisation}/${relativePath}")
                )
            }
        }

        private void copyFile(File from, File to) {
            Files.copy(from.toPath(), to.toPath(), COPY_ATTRIBUTES, REPLACE_EXISTING)
        }

        @SuppressWarnings('CatchException')
        private void downloadFile(URI from, File to) {
            to.parentFile.mkdirs()
            File tmpFile = new File("${to}.tmp")
            try {
                from.toURL().withInputStream { strm ->
                    tmpFile.withOutputStream { output ->
                        output << strm
                    }
                }
                Files.move(tmpFile.toPath(), to.toPath(), ATOMIC_MOVE, REPLACE_EXISTING)
            } catch (Exception e) {
                tmpFile.delete()
                throw e
            }
        }

        private final File repoRoot
        private final Map<String, BinaryRepository> repositories = [:]
    }

    private static class Artifact {
        URI downloadUri
        File destinationPath
    }

    private static String makeRelativePath(String artifactPattern, BinaryDependency dep) {
        IvyPatternHelper.substitute(
            artifactPattern,
            dep.organisation,
            dep.module,
            dep.revision,
            dep.module,
            dep.typeFilter == '*' ? dep.extension : dep.typeFilter,
            dep.extension,
            null,
            null,
            dep.classifier ? ['m:classifier': dep.classifier] : [:]
        )
    }
}
