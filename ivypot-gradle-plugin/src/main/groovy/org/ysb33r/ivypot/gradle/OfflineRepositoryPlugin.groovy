/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2020
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//
package org.ysb33r.ivypot.gradle

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin
import org.ysb33r.ivypot.gradle.internal.plugins.OfflineRepositoryMultiProjectRootPlugin
import org.ysb33r.ivypot.gradle.internal.plugins.OfflineRepositoryMultiProjectSubprojectPlugin
import org.ysb33r.ivypot.gradle.internal.plugins.OfflineRepositorySingleProjectPlugin

/**
 * Install an offline repository management plugin.
 * <p>
 *     It will add additional internal plugins depending on whether the project is a single or multi-project
 * </p>
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class OfflineRepositoryPlugin implements Plugin<Project> {

    public final static String SYNC_TASK_NAME = 'syncRemoteRepositories'
    public final static String GENERATOR_TASK_NAME = 'generateRepoDescriptions'
    public final static String METADATA_GEN_TASK_NAME = 'generateOfflineRepositoryMetadata'
    public final static String REPODETAILS_GEN_TASK_NAME = 'generateOfflineRepositoryDetails'
    public final static String EXTENSION_NAME = 'ivypot'
    public final static String GROUP_NAME = 'Offline Repository'
    public final static String IVY_CONFIGURATION = 'ivypotExecutor'
    public final static String IVY_CONFIGURATION_RESOLVED = IVY_CONFIGURATION + 'Classpath'

    void apply(Project project) {
        project.pluginManager.apply(GrolifantServicePlugin)
        final po = ProjectOperations.find(project)

        if (!po.projectTools.multiProject) {
            project.pluginManager.apply(OfflineRepositorySingleProjectPlugin)
        } else if (po.projectTools.rootProject) {
            project.pluginManager.apply(OfflineRepositoryMultiProjectRootPlugin)
        } else {
            project.pluginManager.apply(OfflineRepositoryMultiProjectSubprojectPlugin)
        }
    }
}
