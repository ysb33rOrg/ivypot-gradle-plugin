/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.extensions

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

import javax.inject.Inject

/**
 * Repository details for a subproject.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultSubprojectRepositoryConfiguration extends AbstractRepositoryConfiguration {
    @Inject
    DefaultSubprojectRepositoryConfiguration(Project project, Provider<File> repoDetailsFile) {
        super(ConfigCacheSafeOperations.from(project))
        updateRoot(repoDetailsFile.map { f ->
            final props = new Properties()
            f.withReader { r ->
                props.load(r)
            }
            new File(props['repo'].toString())
        })
    }
}
