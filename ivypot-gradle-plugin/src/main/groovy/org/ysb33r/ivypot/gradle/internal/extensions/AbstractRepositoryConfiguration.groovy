/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.extensions

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.ivypot.gradle.OfflineRepositoryReadOnlyConfiguration

import static org.ysb33r.ivypot.gradle.internal.utils.RepositoryUtils.fileUri

/**
 * Configures the details of an offline repository
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractRepositoryConfiguration implements OfflineRepositoryReadOnlyConfiguration {

    public static final String SYSPROP_PREFIX = 'org.ysb33r.ivypot.repo'

    /**
     * Location of the offline repository.
     *
     * @return Provider to location.
     */
    @Override
    Provider<File> getRoot() {
        this.root
    }

    /**
     * The URI that points to the offline repository
     * @return Provider to a URI
     */
    @Override
    Provider<URI> getRootUri() {
        this.uri
    }

    /**
     * Repository details as a map of system properties.
     *
     * <p>All properties are prefixed with {@code org.ysb33r.ivypot.repo}.</p>
     *
     * @return Provider to a map of system properties.
     */
    @Override
    Provider<Map<String, String>> getAsSystemProperties() {
        this.sysProps
    }

    protected AbstractRepositoryConfiguration(ConfigCacheSafeOperations ccso) {
        this.stringTools = ccso.stringTools()
        this.root = ccso.providerTools().property(File)
        this.uri = this.root.map { it.absoluteFile.toURI() }
        this.sysProps = ccso.stringTools().provideValues(
            [
                'root.dir'       : root.map { it.absolutePath },
                'root.uri'       : root.map { fileUri(it) },
                'binary.root.dir': binariesRoot.map { it.absolutePath },
                'binary.root.uri': binariesRoot.map { fileUri(it) },
                'groovy.dsl.file': groovyRepository.map { it.absolutePath },
                'groovy.dsl.uri' : groovyRepository.map { fileUri(it) },
                'kotlin.dsl.file': kotlinRepository.map { it.absolutePath },
                'kotlin.dsl.uri' : kotlinRepository.map { fileUri(it) }
            ].collectEntries {
                ["${SYSPROP_PREFIX}.${it.key}".toString(), it.value]
            }
        )
    }

    protected  final StringTools stringTools

    protected void updateRoot(Provider<File> newRoot) {
        this.root.set(newRoot)
    }

    private final Property<File> root
    private final Provider<URI> uri
    private final Provider<Map<String, String>> sysProps
}
