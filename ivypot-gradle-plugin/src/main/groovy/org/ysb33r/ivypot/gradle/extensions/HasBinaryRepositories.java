/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.extensions;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.provider.Provider;
import org.ysb33r.ivypot.gradle.BinaryRepositoryDescriptor;
import org.ysb33r.ivypot.gradle.BinaryRepositorySourceSpec;

import java.util.Map;

/**
 * States that binary repositories can be configured.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface HasBinaryRepositories {

    /**
     * Collection of all declared binary repositories.
     *
     * @return Provider to a mapping of binary repositories.
     */
    Provider<Map<String, BinaryRepositoryDescriptor>> getBinaryRepositories();

    /**
     * Configures binary repositories.
     *
     * @param action configurator
     */
    void binaryRepositories(Action<? super BinaryRepositorySourceSpec> action);

    /**
     * Configures binary repositories.
     *
     * @param configurator Configurator
     */
    void binaryRepositories(@DelegatesTo(BinaryRepositorySourceSpec.class) Closure<?> configurator);
}
