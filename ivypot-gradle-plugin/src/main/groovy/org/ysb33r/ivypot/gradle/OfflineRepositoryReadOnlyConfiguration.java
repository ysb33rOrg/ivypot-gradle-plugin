/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle;

import org.gradle.api.provider.Provider;
import org.ysb33r.ivypot.remote.BinaryExecutor;

import java.io.File;
import java.net.URI;
import java.util.Map;

/**
 * Configures an offline repository.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface OfflineRepositoryReadOnlyConfiguration {

    /**
     * Location of the offline repository.
     *
     * @return Provider to location.
     */
    Provider<File> getRoot();

    /**
     * Top directory where binaries are downloaded to.
     *
     * @return Provider to location
     */
    default Provider<File> getBinariesRoot() {
        return getRoot().map( x -> new File(x, BinaryExecutor.BINARY_SUBDIR));
    }

    /**
     * Directory where binaries for a specific binary repository is downloaded to.
     *
     * <p>
     *     This can be used in test system properties to provide a location.
     * </p>
     * @param repositoryName Name of binary repository
     * @return Download location.
     */
    default Provider<File> getBinariesRootFor(String repositoryName) {
        return getBinariesRoot().map( x -> new File(x, repositoryName));
    }

    /**
     * The URI that points to the offline repository
     * @return Provider to a URI
     */
    Provider<URI> getRootUri();

    /**
     * Location of a file in Groovy DSL format that describes the offline Ivy repository.
     * @return Provider to a generated Gradle file.
     */
    default Provider<File> getGroovyRepository() {
        return getRoot().map( f -> new File(f, "repository.gradle"));
    }

    /**
     * Location of a file in Kotlin DSL format that describes the offline Ivy repository.
     * @return Provider to a generated Gradle file.
     */
    default Provider<File> getKotlinRepository() {
        return getRoot().map( f -> new File(f, "repository.gradle.kts"));
    }

    /**
     * Repository details as a map of system properties.
     *
     * <p>All properties are prefixed with {@code org.ysb33r.ivypot.repo}.</p>
     *
     * @return Provider to a map of system properties.
     */
    Provider<Map<String,String>> getAsSystemProperties();
}
