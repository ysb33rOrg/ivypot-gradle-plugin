/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2020
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//
package org.ysb33r.ivypot.gradle.dependencies

import groovy.transform.CompileStatic
import org.gradle.api.GradleException
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ResolvedArtifact

import javax.annotation.Nullable

/**
 * Contains information on a resolved dependency
 *
 * @since 2.0
 */
@CompileStatic
class ResolvedDependency implements Dependency {

    ResolvedDependency(ResolvedArtifact artifact, boolean transitive) {
        this.resolved = artifact
        this.transitive = transitive
    }

    @Override
    String getGroup() {
        resolved.moduleVersion.id.group
    }

    @Override
    String getName() {
        resolved.moduleVersion.id.name
    }

    @Override
    String getVersion() {
        resolved.moduleVersion.id.version
    }

    @Override
    boolean contentEquals(Dependency dependency) {
        return false
    }

    @Override
    Dependency copy() {
        throw new GradleException('This object cannot be copied')
    }

    final String reason = null

    @Override
    void because(@Nullable String s) {
    }

    @SuppressWarnings('UnnecessaryCast')
    Map<String,String> asMap() {
        final map = new TreeMap<String,String>()
        map.putAll([
            organisation: group,
            module      : name,
            revision    : version,
            transitive  : transitive,
            typeFilter  : resolved.type ?: WILDCARD,
            confFilter  : WILDCARD,
            classifier  : resolved.classifier,
            extension   : resolved.extension
        ] as Map<? extends String, ? extends String>)
        map
    }

    private final ResolvedArtifact resolved
    private final boolean transitive
    private static final String WILDCARD = '*'
}
