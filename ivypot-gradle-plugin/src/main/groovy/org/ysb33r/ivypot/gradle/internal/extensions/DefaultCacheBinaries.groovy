/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.extensions

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.artifacts.ModuleDependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Provider
import org.ysb33r.ivypot.gradle.CacheBinaries
import org.ysb33r.ivypot.gradle.errors.BadBinaryException
import org.ysb33r.ivypot.remote.BinaryDependency

/**
 * Default implementation of {@link CacheBinaries}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultCacheBinaries implements CacheBinaries {

    DefaultCacheBinaries(Project project) {
        this.binaries = project.objects.listProperty(BinaryDependency)
        this.dependencyHandler = project.dependencies
    }

    /**
     * Adds one of more binaries in the form {@code "group:name:version:classifier@ext"}.
     * The group name must match a defined binary repository.
     *
     * @param pattern One or more binaries by Ivy pattern.
     */
    @Override
    void add(String... pattern) {
        this.binaries.addAll(pattern.collect { convert(it) })
    }

    /**
     * The list of binary declarations.
     *
     * @return Provider to list of binary declarations.
     */
    @Override
    Provider<List<BinaryDependency>> getBinaryDeclaration() {
        this.binaries
    }

    private BinaryDependency convert(String notation) {
        final dep = (ModuleDependency) dependencyHandler.create(notation)
        final artifacts = dep.artifacts

        if (artifacts.empty) {
            throw new BadBinaryException("'${notation}' is missing an extension")
        }
        final artifact = dep.artifacts.first()

        new BinaryDependency(
            organisation: dep.group,
            module: dep.name,
            revision: dep.version,
            typeFilter: artifact.type ?: '*',
            classifier: artifact.classifier,
            extension: artifact.extension
        )
    }

    private final ListProperty<BinaryDependency> binaries
    private final DependencyHandler dependencyHandler
}
