/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.ivypot.gradle.extensions.OfflineRepositorySingleProjectExtension
import org.ysb33r.ivypot.gradle.internal.utils.SyncUtils
import org.ysb33r.ivypot.remote.BinaryRepository

import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.EXTENSION_NAME
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.addTestSysPropProvider

/**
 * The plugin that is applied for a single project layout.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OfflineRepositorySingleProjectPlugin implements Plugin<Project> {
    @Override
    @SuppressWarnings('UnnecessaryObjectReferences')
    void apply(Project project) {
        final ext = project.extensions.create(EXTENSION_NAME, OfflineRepositorySingleProjectExtension, project)
        final sync = SyncUtils.registerSyncTask(project)
        SyncUtils.registerRepoGeneratorTask(project, ext.repoDetails)
        SyncUtils.registerWorkerConfiguration(project)

        sync.configure {
            it.credentials = ext.credentials.credentialMetadata
            it.dependencies = ext.configurations.mappedDependencies
            it.repositoryData = ext.repositories.allowedRepositoryMetadata
            it.repoRoot = ext.repoDetails.root
            it.binaryDependencies = ext.cacheBinaries.binaryDeclaration
            it.binaryRepositoryData = ext.binaryRepositories.map {
                it.collectEntries { k, v ->
                    [
                        k,
                        new BinaryRepository(
                            artifactPattern: v.artifactPattern,
                            rootUri: v.rootUri
                        )
                    ]
                } as Map<String, BinaryRepository>
            }
        }

        addTestSysPropProvider(project, ext.repoDetails)
    }
}
