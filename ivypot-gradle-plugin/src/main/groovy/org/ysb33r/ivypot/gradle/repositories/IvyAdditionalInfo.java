/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.repositories;

/**
 *
 * This was born out of the problem that it is hard to extract certain information out of the Gradle API.
 *
 * @author Schalk W.Cronjé
 *
 * @since 2.0
 */
public interface IvyAdditionalInfo {

    /**
     * Ivy descriptor patterns.
     *
     * @param patterns One or more patterns.
     */
    void ivyPatterns(String... patterns);

    /**
     * Artifact patterns.
     *
     * @param patterns one or more patterns.
     */
    void artifactPatterns(String... patterns);

    /**
     * Mark repository as artifact-only.
     *
     * <p>
     *     This is useful when Ivy is used to pull down arbitrary binaries.
     * </p>
     */
    void artifactOnly();
}
