/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle;

import org.gradle.api.provider.Provider;
import org.ysb33r.ivypot.remote.BinaryDependency;

import java.util.List;
import java.util.Set;

/**
 * Interface for adding binaries to be cached
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface CacheBinaries {
    /**
     * Adds one of more binaries in the form "{@code group:name:version:classifier@ext}.
     * The group name must match a defined binary repository.
     *
     * @param pattern One of more binaries.
     */
    void add(String... pattern);

    /**
     * The list of binary declarations.
     *
     * @return Provider to list of binary declarations.
     */
    Provider<List<BinaryDependency>> getBinaryDeclaration();
}
