/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle;

/**
 * Defines a binary repository.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface BinaryRepositorySpec {
    /**
     * The root URI where to download binaries from.
     *
     * @param uri Anything convertible to a URI using {@link org.ysb33r.grolifant5.api.core.StringTools#urize}.
     */
    void setRootUri(Object uri);

    /**
     * Provide an Ivy-Gradle artifact pattern that can translate a cached binary string into a downloadable URI.
     *
     * <p>
     * For instance: {@code 'v[revision]/[module]-v[revision]-[classifier].[ext]'}.
     * </p>
     *
     * @param pattern Anything convertible to a string using
     *                c{@link org.ysb33r.grolifant5.api.core.StringTools#stringize}.
     */
    void setArtifactPattern(Object pattern);
}
