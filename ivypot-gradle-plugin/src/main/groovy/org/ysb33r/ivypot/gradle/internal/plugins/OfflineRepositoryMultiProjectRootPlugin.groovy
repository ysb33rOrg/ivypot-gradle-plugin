/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.attributes.Attribute
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.ivypot.gradle.OfflineRepositorySync
import org.ysb33r.ivypot.gradle.extensions.OfflineRepositoryMultiProjectRootExtension
import org.ysb33r.ivypot.gradle.internal.utils.SyncUtils
import org.ysb33r.ivypot.remote.BinaryRepository

import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.EXTENSION_NAME
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.IVYPOT_ATTRIBUTE_NAME
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.IVYPOT_ATTRIBUTE_VALUE
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.METADATA_CONFIG_INCOMING
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.METADATA_CONFIG_RESOLVE

/**
 * The plugin that is applied for the root project in a multi-project layout.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OfflineRepositoryMultiProjectRootPlugin implements Plugin<Project> {
    public static final String REPO_DETAILS_TASK = ''

    @Override
    @SuppressWarnings('UnnecessaryObjectReferences')
    void apply(Project project) {
        final ext = project.extensions.create(
            EXTENSION_NAME,
            OfflineRepositoryMultiProjectRootExtension,
            project
        )

        SyncUtils.declareIvypotAttributes(project)

        createIncomingMetadataConfigurations(project)
        createOutgoingRepoDetailsConfiguration(project, ext)

        final incomingMetadataFiles = project.configurations.getByName(METADATA_CONFIG_RESOLVE)
            .incoming.artifactView { av ->
            av.attributes { attrs ->
                attrs.attribute(
                    Attribute.of(IVYPOT_ATTRIBUTE_NAME, String),
                    IVYPOT_ATTRIBUTE_VALUE
                )
            }
        }.files

        final sync = SyncUtils.registerSyncTask(project)
        configureSyncTask(sync, incomingMetadataFiles, ext)
        SyncUtils.registerRepoGeneratorTask(project, ext.repoDetails)
        SyncUtils.registerWorkerConfiguration(project)
    }

    private void createIncomingMetadataConfigurations(Project project) {
        ProjectOperations.find(project)
            .configurations.createLocalRoleFocusedConfiguration(
            METADATA_CONFIG_INCOMING,
            METADATA_CONFIG_RESOLVE,
            false
        )
    }

    private void createOutgoingRepoDetailsConfiguration(
        Project project,
        OfflineRepositoryMultiProjectRootExtension ext
    ) {
        final task = SyncUtils.registerRepoDetailsTask(project)
        task.configure {
            it.location = ext.repoDetails.root
        }
        SyncUtils.declareOutgoingRepoDetailsConfiguration(project, task)
    }

    private void configureSyncTask(
        TaskProvider<OfflineRepositorySync> sync,
        FileCollection incomingMetadataFiles,
        OfflineRepositoryMultiProjectRootExtension ext
    ) {
        sync.configure { t ->
            t.tap {
                inputs.files(incomingMetadataFiles)
                credentials = ext.credentials.credentialMetadata
                repoRoot = ext.repoDetails.root
                metadataFiles(incomingMetadataFiles)
                binaryRepositoryData = ext.binaryRepositories.map { m ->
                    m.collectEntries { k, v ->
                        [
                            k,
                            new BinaryRepository(
                                artifactPattern: v.artifactPattern,
                                rootUri: v.rootUri
                            )
                        ]
                    } as Map<String, BinaryRepository>
                }
            }
        }
    }
}
