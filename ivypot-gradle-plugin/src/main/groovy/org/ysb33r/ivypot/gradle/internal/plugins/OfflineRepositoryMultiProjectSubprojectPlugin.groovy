/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.TaskProvider
import org.gradle.api.tasks.testing.Test
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.ivypot.gradle.OfflineRepositoryRepoConsumer
import org.ysb33r.ivypot.gradle.extensions.OfflineRepositoryMultiProjectSubprojectExtension
import org.ysb33r.ivypot.gradle.internal.utils.SyncUtils

import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.EXTENSION_NAME
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.REPODETAILS_CONFIG_INCOMING
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.REPODETAILS_CONFIG_OUTGOING
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.REPODETAILS_CONFIG_RESOLVE
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.addTestSysPropProvider
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.declareIvypotAttributes
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.declareOutgoingMetadataConfiguration

/**
 * The plugin that is applied for a subproject in a multi-project layout.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OfflineRepositoryMultiProjectSubprojectPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        final incoming = createIncomingRepoDetailsConfigurations(project)

        final ext = project.extensions.create(
            EXTENSION_NAME,
            OfflineRepositoryMultiProjectSubprojectExtension,
            project,
            incoming.flatMap { it.repoLocation }
        )

        final task = SyncUtils.registerMetadataTask(project)
        task.configure { t ->
            t.dependencies = ext.configurations.mappedDependencies
            t.binaryDependencies = ext.cacheBinaries.binaryDeclaration
            t.repositoryData = ext.repositories.allowedRepositoryMetadata
        }

        declareIvypotAttributes(project)
        declareOutgoingMetadataConfiguration(project, task)
        addTestSysPropProvider(project, ext.repoDetails)
    }

    private TaskProvider<OfflineRepositoryRepoConsumer> createIncomingRepoDetailsConfigurations(Project project) {
        ProjectOperations.find(project)
            .configurations.createLocalRoleFocusedConfiguration(
            REPODETAILS_CONFIG_INCOMING,
            REPODETAILS_CONFIG_RESOLVE,
            false
        )

        project.dependencies.add(
            REPODETAILS_CONFIG_INCOMING,
            project.dependencies.project(path: ':', configuration: REPODETAILS_CONFIG_OUTGOING)
        )

        final sync = project.tasks.register('ivypotRepoDetailsSync', OfflineRepositoryRepoConsumer)
        project.tasks.withType(Test).configureEach { task ->
            task.dependsOn(sync)
        }

        sync
    }
}
