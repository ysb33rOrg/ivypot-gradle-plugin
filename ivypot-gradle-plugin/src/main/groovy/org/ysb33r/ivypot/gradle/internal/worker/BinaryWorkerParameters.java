/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.worker;

import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.MapProperty;
import org.gradle.api.provider.Property;
import org.gradle.workers.WorkParameters;
import org.ysb33r.ivypot.remote.BinaryDependency;
import org.ysb33r.ivypot.remote.BinaryRepository;

/**
 * Parameters for executing binary downloads.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface BinaryWorkerParameters extends WorkParameters {
    DirectoryProperty getOutputDirectory();

    ListProperty<BinaryDependency> getBinaries();

    MapProperty<String, BinaryRepository> getRepositories();

    Property<Boolean> getRefreshDependencies();
}
