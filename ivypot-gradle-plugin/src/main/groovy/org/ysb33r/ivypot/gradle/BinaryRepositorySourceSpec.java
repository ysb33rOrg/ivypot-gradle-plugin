/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;

/**
 * For declaring new binary repositories
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface BinaryRepositorySourceSpec {
    /**
     * Configures a repository. If it does not exist, it is added.
     *
     * @param name Name of repository.
     * @param configurator Configurator.
     */
    void source(String name, Action<? super BinaryRepositorySpec> configurator);

    /**
     * Configures a repository. If it does not exist, it is added.
     *
     * @param name Name of repository.
     * @param configurator Configurator.
     */
    void source(String name, @DelegatesTo(BinaryRepositorySpec.class) Closure<?> configurator);
}
