/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.extensions

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.ivypot.gradle.CredentialProviders
import org.ysb33r.ivypot.gradle.CredentialsConfigurator

import javax.inject.Inject

/**
 * Default credentials declaration.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultCredentials implements CredentialsConfigurator, CredentialProviders, Serializable {

    @Inject
    DefaultCredentials(Project project) {
        this.stringTools = ConfigCacheSafeOperations.from(project).stringTools()
        this.username = project.objects.property(String)
        this.password = project.objects.property(String)
        this.realm = project.objects.property(String)
    }

    @Override
    void setUsername(Object value) {
        stringTools.updateStringProperty(this.username, value)
    }

    @Override
    void setPassword(Object value) {
        stringTools.updateStringProperty(this.password, value)
    }

    @Override
    void setRealm(Object value) {
        stringTools.updateStringProperty(this.realm, value)
    }

    @Override
    Provider<String> getUsername() {
        this.username
    }

    @Override
    Provider<String> getPassword() {
        this.password
    }

    @Override
    Provider<String> getRealm() {
        this.realm
    }

    private final StringTools stringTools
    private final Property<String> username
    private final Property<String> password
    private final Property<String> realm
}
