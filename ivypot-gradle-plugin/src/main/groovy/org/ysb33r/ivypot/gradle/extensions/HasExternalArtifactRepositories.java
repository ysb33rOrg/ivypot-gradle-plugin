/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.extensions;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.ysb33r.ivypot.gradle.ExternalArtifactRepositories;

/**
 * Define repositories queried for offline caching.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
public interface HasExternalArtifactRepositories {

    ExternalArtifactRepositories getRepositories();

    void repositories(Action<ExternalArtifactRepositories> configurator);

    void repositories(@DelegatesTo(ExternalArtifactRepositories.class) Closure<?> configurator);
}
