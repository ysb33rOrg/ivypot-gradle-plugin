/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.repositories

import org.gradle.api.Project
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.ivypot.gradle.BinaryRepositorySpec
import org.ysb33r.ivypot.gradle.BinaryRepositoryDescriptor

import javax.inject.Inject

/**
 * Default implementation of binary repository declaration.
 *
 * @author Schalk W. Cornjé
 *
 * @since 2.0
 */
class DefaultBinaryRepository implements BinaryRepositorySpec, BinaryRepositoryDescriptor, Serializable {

    final String name

    @Inject
    DefaultBinaryRepository(Project project, String name) {
        this.name = name
        this.stringTools = ConfigCacheSafeOperations.from(project).stringTools()
    }

    /**
     * The root URI of a binary location.
     *
     * @return URI
     */
    @Override
    URI getRootUri() {
       stringTools.urize(this.uri)
    }

    /**
     * Ivy-style artifact pattern.
     *
     * @return Binary pattern.
     */
    @Override
    String getArtifactPattern() {
       stringTools.stringize(this.pattern)
    }

    /**
     * The root URI where to download binaries from.
     *
     * @param uri Anything convertible to a URI using {@link org.ysb33r.grolifant5.api.core.StringTools#urize}.
     */
    @Override
    void setRootUri(Object uri) {
        this.uri = uri
    }

    /**
     * Provide an Ivy-Gradle artifact pattern that can translate a cached binary string into a downloadable URI.
     *
     * <p>
     * For instance: {@code 'v[revision]/[module]-v[revision]-[classifier].[ext]'}.
     * </p>
     *
     * @param pattern Anything convertible to a string using
     * {@link org.ysb33r.grolifant5.api.core.StringTools#stringize}.
     */
    @Override
    void setArtifactPattern(Object pattern) {
        this.pattern = pattern
    }

    private final StringTools stringTools
    private Object uri
    private Object pattern
}
