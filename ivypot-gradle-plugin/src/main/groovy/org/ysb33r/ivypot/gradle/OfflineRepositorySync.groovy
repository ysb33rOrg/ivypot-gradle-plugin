/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2020
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//
package org.ysb33r.ivypot.gradle

import groovy.transform.CompileStatic
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.TaskAction
import org.gradle.workers.WorkerExecutor
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask
import org.ysb33r.ivypot.gradle.internal.data.SubprojectMetadata
import org.ysb33r.ivypot.gradle.internal.utils.IvyUtils
import org.ysb33r.ivypot.gradle.internal.worker.BinaryWorkExecutor
import org.ysb33r.ivypot.gradle.internal.worker.IvyWorkExecutor
import org.ysb33r.ivypot.gradle.repositories.RepositoryMetadata
import org.ysb33r.ivypot.remote.BinaryDependency
import org.ysb33r.ivypot.remote.BinaryRepository
import org.ysb33r.ivypot.remote.IvyDependency

import javax.inject.Inject

import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.IVY_CONFIGURATION_RESOLVED

/**
 * The repository synchronisation task.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class OfflineRepositorySync extends GrolifantDefaultTask {

    @Inject
    OfflineRepositorySync(WorkerExecutor we) {
        this.refreshDependencies = project.gradle.startParameter.refreshDependencies
        this.workerExecutor = we
        this.dependencySet = project.objects.listProperty(IvyDependency)
        this.binaries = project.objects.listProperty(BinaryDependency)
        this.repositories = project.objects.mapProperty(String, RepositoryMetadata)
        this.classpath = project.configurations.getByName(IVY_CONFIGURATION_RESOLVED)
        this.rootProvider = project.objects.directoryProperty()
        this.settingsFile = rootProvider.map { it.file('ivysettings.xml').asFile }
        this.creds = project.objects.mapProperty(String, CredentialProviders)
        this.binaryRepositories = project.objects.mapProperty(String, BinaryRepository)
        this.metadataFileCollection = fsOperations().emptyFileCollection()
        this.metadataFiles = metadataFileCollection.elements.map { set -> set*.asFile }
        this.metadataProvider = this.metadataFiles.map { SubprojectMetadata.readMetadata(it) }

        this.cacheDir = fsOperations().buildDirDescendant(
            "tmp/ivypot/${fsOperations().toSafeFileName(name)}"
        )

        final credsAnalyzed = creds.zip(this.repositories) { credMap, repoMap ->
            IvyUtils.extractCredentialsMappings(credMap, repoMap)
        }

        this.credsProvider = credsAnalyzed.map { initCredentials(it) }
        this.sysPropProvider = credsAnalyzed.map { initSystemProperties(it) }

        configureInputsAndOutputs()
    }

    void setRepoRoot(Object dir) {
        rootProvider.set(project.layout.dir(fsOperations().provideFile(dir)))
    }

    @InputFiles
    FileCollection getClasspath() {
        this.classpath
    }

    void setDependencies(Provider<List<IvyDependency>> data) {
        this.dependencySet.set(data)
    }

    void setRepositoryData(Provider<Map<String, RepositoryMetadata>> data) {
        this.repositories.set(data)
    }

    void setCredentials(Provider<Map<String, CredentialProviders>> creds) {
        this.creds.set(creds)
    }

    void setBinaryDependencies(Provider<List<BinaryDependency>> data) {
        this.binaries.set(data)
    }

    void setBinaryRepositoryData(Provider<Map<String, BinaryRepository>> data) {
        this.binaryRepositories.set(data)
    }

    void metadataFiles(FileCollection files) {
        this.metadataFileCollection.from(files)
    }

    @TaskAction
    void exec() {
        final deps = dependencySet.get()
        final sysProps = sysPropProvider.get()
        final hasCredentials = !sysProps.isEmpty()
        final refresh = this.refreshDependencies
        final binaryDeps = this.binaries.get()
        final binaryRepos = this.binaryRepositories.get()
        final fromMetadata = metadataProvider.get()

        writeSettingsFile(fromMetadata)
        final wq = hasCredentials ? workerExecutor.processIsolation { pws ->
            pws.forkOptions {
                it.systemProperties(sysProps)
            }
            pws.classpath.from(owner.classpath)
        } : workerExecutor.classLoaderIsolation { cws ->
            cws.classpath.from(owner.classpath)
        }

        wq.submit(
            IvyWorkExecutor, params -> {
            params.tap {
                outputDirectory.set(rootProvider)
                dependencies.set(deps + fromMetadata.dependencies)
                ivySettingsXml.set(settingsFile)
                refreshDependencies.set(refresh)
            }
        })

        wq.submit(
            BinaryWorkExecutor, params -> {
            params.tap {
                outputDirectory.set(rootProvider)
                refreshDependencies.set(refresh)
                binaries.set(binaryDeps + fromMetadata.binaries)
                repositories.set(binaryRepos)
            }
        }
        )
    }

    private File writeSettingsFile(SubprojectMetadata metadata) {
        IvyUtils.writeSettingsFile(
            settingsFile.get(),
            rootProvider.get().asFile,
            cacheDir.get(),
            repositories.get() + metadata.repositories,
            credsProvider.get()
        )
    }

    private Set<Map<String, String>> initCredentials(List<Map<String, String>> set) {
        set.collect { map ->
            map.findAll { !it.key.endsWith('Value') }
                .collectEntries { k, v ->
                    if (k.endsWith('Prop')) {
                        [
                            k.replaceFirst(~/Prop/, ''),
                            '${' + v + '}'
                        ]
                    } else {
                        [k, v]
                    }
                } as Map<String, String>
        }.toSet()
    }

    private Map<String, String> initSystemProperties(List<Map<String, String>> set) {
        final result = new TreeMap<String, String>()
        set.each {
            result.putAll(
                [
                    (it['usernameProp']): it['usernameValue'],
                    (it['passwordProp']): it['passwordValue']
                ])
        }
        result
    }

    private void configureInputsAndOutputs() {
        outputs.file(this.settingsFile)
        inputs.files(this.metadataFileCollection)
        inputs.tap {
            property('repositories', this.repositories)
            property('binaryRepositories', this.binaryRepositories)
            property('binaries', this.binaries)
            property('dependencies', this.dependencySet)
            property('metadata', this.metadataProvider)
        }
    }

    private final boolean refreshDependencies
    private final WorkerExecutor workerExecutor
    private final ListProperty<IvyDependency> dependencySet
    private final ListProperty<BinaryDependency> binaries
    private final MapProperty<String, RepositoryMetadata> repositories
    private final MapProperty<String, BinaryRepository> binaryRepositories
    private final FileCollection classpath
    private final DirectoryProperty rootProvider
    private final Provider<File> settingsFile
    private final Provider<File> cacheDir
    private final MapProperty<String, CredentialProviders> creds
    private final Provider<Set<Map<String, String>>> credsProvider
    private final Provider<Map<String, String>> sysPropProvider
    private final ConfigurableFileCollection metadataFileCollection
    private final Provider<List<File>> metadataFiles
    private final Provider<SubprojectMetadata> metadataProvider
}
