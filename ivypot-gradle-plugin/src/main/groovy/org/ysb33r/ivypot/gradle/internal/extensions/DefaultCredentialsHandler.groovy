/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.extensions

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Provider
import org.ysb33r.ivypot.gradle.CredentialProviders
import org.ysb33r.ivypot.gradle.CredentialsConfigurator
import org.ysb33r.ivypot.gradle.CredentialsHandler

import javax.inject.Inject
import java.util.concurrent.Callable

import static org.ysb33r.grolifant5.api.core.ClosureUtils.configureItem

/**
 * Default implementation of credentials declaration.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultCredentialsHandler implements CredentialsHandler {

    @Inject
    DefaultCredentialsHandler(Project project) {
        this.objectFactory = project.objects
        this.creds = [:]
        this.credsProvider = project.provider({ Map<String, DefaultCredentials> map ->
            map.collectEntries { k, v ->
                [k, (CredentialProviders) v]
            }.asImmutable()
        }.curry(this.creds) as Callable<Map<String, CredentialProviders>>)
    }

    @Override
    void forName(String name, Action<CredentialsConfigurator> configurator) {
        if (!this.creds.containsKey(name)) {
            this.creds[name] = objectFactory.newInstance(DefaultCredentials)
        }

        configurator.execute(this.creds[name])
    }

    @Override
    void forName(String name, @DelegatesTo(CredentialsConfigurator) Closure<?> configurator) {
        if (!this.creds.containsKey(name)) {
            this.creds[name] = objectFactory.newInstance(DefaultCredentials)
        }

        configureItem(this.creds[name], configurator)
    }

    @Override
    Provider<Map<String, CredentialProviders>> getCredentialMetadata() {
        this.credsProvider
    }

    private final Map<String, DefaultCredentials> creds
    private final Provider<Map<String, CredentialProviders>> credsProvider
    private final ObjectFactory objectFactory
}
