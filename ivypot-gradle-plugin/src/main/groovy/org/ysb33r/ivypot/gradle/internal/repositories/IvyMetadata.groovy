/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.repositories

import groovy.transform.CompileStatic
import org.ysb33r.ivypot.gradle.repositories.IvyAdditionalInfo

/**
 * Ivy repository metadata.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class IvyMetadata implements IvyAdditionalInfo {
    /**
     * Ivy descriptor patterns.
     *
     * @param patterns One or more patterns.
     */
    @Override
    void ivyPatterns(String... patterns) {
        this.ivyPats.addAll(patterns)
    }

    /**
     * Artifact patterns.
     *
     * @param patterns one or more patterns.
     */
    @Override
    void artifactPatterns(String... patterns) {
        this.artifactPats.addAll(patterns)
    }

    /**
     * Mark repository as artifact-only.
     *
     * <p>
     *     This is useful when Ivy is used to pull down arbitrary binaries.
     * </p>
     */
    @Override
    void artifactOnly() {
        this.artifactOny = true
        this.ivyPats.clear()
    }

    boolean getArtifactOnly() {
        this.artifactOnly
    }

    Iterable<String> getArtifactPatterns() {
        this.artifactPats
    }

    Iterable<String> getIvyPatterns() {
        this.artifactPats
    }

    private final List<String> ivyPats = []
    private final List<String> artifactPats = []
    private boolean artifactOny
}
