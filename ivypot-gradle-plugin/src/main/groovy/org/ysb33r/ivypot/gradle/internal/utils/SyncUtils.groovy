/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.utils

import groovy.transform.CompileStatic
import groovy.transform.Memoized
import org.gradle.api.Project
import org.gradle.api.attributes.Attribute
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskProvider
import org.gradle.api.tasks.testing.Test
import org.gradle.process.CommandLineArgumentProvider
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.ivypot.gradle.OfflineRepositoryConfiguration
import org.ysb33r.ivypot.gradle.OfflineRepositoryMetadata
import org.ysb33r.ivypot.gradle.OfflineRepositoryReadOnlyConfiguration
import org.ysb33r.ivypot.gradle.OfflineRepositoryRepoDetailsGenerator
import org.ysb33r.ivypot.gradle.OfflineRepositorySync
import org.ysb33r.ivypot.gradle.RepositoryGenerator

import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.GENERATOR_TASK_NAME
import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.GROUP_NAME
import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.IVY_CONFIGURATION
import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.IVY_CONFIGURATION_RESOLVED
import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.METADATA_GEN_TASK_NAME
import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.REPODETAILS_GEN_TASK_NAME
import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.SYNC_TASK_NAME

/**
 * Internal utilities for configuring the plugin.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@SuppressWarnings('DuplicateStringLiteral')
class SyncUtils {

    /**
     * Name of dependency attribute to share Ivypot metadata within a multi-project.
     */
    public static final String IVYPOT_ATTRIBUTE_NAME = 'org.ysb33r.ivypot.metadata'
    public static final String IVYPOT_ATTRIBUTE_VALUE = 'metadata'

    public static final String METADATA_CONFIG_BASENAME = '$$$IvyPot'
    public static final String METADATA_CONFIG_INCOMING = METADATA_CONFIG_BASENAME + 'Declaration$$$'
    public static final String METADATA_CONFIG_RESOLVE = METADATA_CONFIG_BASENAME + 'Resolver$$$'
    public static final String METADATA_CONFIG_OUTGOING = METADATA_CONFIG_BASENAME + 'Outgoing$$$'

    public static final String REPODETAILS_CONFIG_BASENAME = '$$$IvypotRepoDetails'
    public static final String REPODETAILS_CONFIG_INCOMING = REPODETAILS_CONFIG_BASENAME + 'Declaration$$$'
    public static final String REPODETAILS_CONFIG_RESOLVE = REPODETAILS_CONFIG_BASENAME + 'Resolver$$$'
    public static final String REPODETAILS_CONFIG_OUTGOING = REPODETAILS_CONFIG_BASENAME + 'Outgoing$$$'

    public static final String REPODETAILS_FILENAME = 'details.properties'

    /**
     * Registers a sync task.
     *
     * @param project Associated project
     * @return Task provider
     */
    static TaskProvider<OfflineRepositorySync> registerSyncTask(Project project) {
        project.tasks.register(SYNC_TASK_NAME, OfflineRepositorySync) { t ->
            t.tap {
                description = 'Populates an offline repository'
                group = GROUP_NAME
                dependsOn(GENERATOR_TASK_NAME)
            }
        }
    }

    /**
     * Registers a task that will generate repository details information.
     *
     * @param project Associated project
     * @return Task provider
     */
    static TaskProvider<OfflineRepositoryRepoDetailsGenerator> registerRepoDetailsTask(Project project) {
        project.tasks.register(REPODETAILS_GEN_TASK_NAME, OfflineRepositoryRepoDetailsGenerator) { t ->
            t.tap {
                description = 'Generates details about this offline repository'
                group = GROUP_NAME
                dependsOn(SYNC_TASK_NAME)
            }
        }
    }
    /**
     * Registers an Ivypot metadata task.
     *
     * @param project Associated project
     * @return Task provider
     */
    static TaskProvider<OfflineRepositoryMetadata> registerMetadataTask(Project project) {
        project.tasks.register(METADATA_GEN_TASK_NAME, OfflineRepositoryMetadata) { t ->
            t.tap {
                description = 'Generate Ivypoot metadata'
                group = GROUP_NAME
            }
        }
    }
    /**
     * Registers a sync task.
     *
     * @param project Associated project
     * @return Task provider
     */
    static TaskProvider<RepositoryGenerator> registerRepoGeneratorTask(
        Project project,
        OfflineRepositoryConfiguration orc
    ) {
        project.tasks.register(GENERATOR_TASK_NAME, RepositoryGenerator) { t ->
            t.tap {
                description = 'Generates repository DSL files'
                group = GROUP_NAME
                groovyRepository = orc.groovyRepository
                kotlinRepository = orc.kotlinRepository
                repositoryUri = orc.rootUri
            }
        }
    }

    static void registerWorkerConfiguration(Project project) {
        final po = ProjectOperations.find(project)
        po.configurations.createLocalRoleFocusedConfiguration(IVY_CONFIGURATION, IVY_CONFIGURATION_RESOLVED)

        InputStream strm = SyncUtils.classLoader.getResourceAsStream('ivypot/versions.properties')
        final versions = strm.withCloseable {
            Properties props = new Properties()
            props.load(strm)
            props
        }

//        final versions = po.fsOperations.loadPropertiesFromResource('ivypot/versions.properties')
        project.dependencies.add(
            IVY_CONFIGURATION,
            "org.apache.ivy:ivy:${versions['ivy']}"
        )
    }

    @SuppressWarnings(['UnnecessaryObjectReferences', 'DuplicateStringLiteral'])
    static void writeRepositoryFiles(
        Provider<URI> repoUri,
        Provider<File> repositoryGroovy,
        Provider<File> repositoryKotlin
    ) {
        final uri = repoUri.get().toString()

        repositoryGroovy.get().withWriter { w ->
            w.println 'repositories {'
            w.print ' ' * 4
            w.println 'ivy {'
            w.print ' ' * 8
            w.println "name = '${REPO_NAME}'"
            w.print ' ' * 8
            w.println "layout 'gradle'"
            w.print ' ' * 8
            w.println "url = '${uri}'"
            w.print ' ' * 4
            w.println '}'
            w.println '}'
        }

        repositoryKotlin.get().withWriter { w ->
            w.println 'repositories {'
            w.print ' ' * 4
            w.println 'ivy {'
            w.print ' ' * 8
            w.println "name = \"${REPO_NAME}\""
            w.print ' ' * 8
            w.println 'layout("gradle")'
            w.print ' ' * 8
            w.println "url = uri(\"${uri}\")"
            w.print ' ' * 4
            w.println '}'
            w.println '}'
        }
    }

    /**
     * Declares the Ivypot attrovutes on the dependency handler.
     *
     * @param project Associated project.
     */
    static void declareIvypotAttributes(Project project) {
        project.dependencies.attributesSchema { schema ->
            schema.attribute(Attribute.of(IVYPOT_ATTRIBUTE_NAME, String))
        }
    }

    /**
     * Loads all projects recursively.
     *
     * @param project Starting project.
     * @return All subprojects found from the given project.
     */
    @Memoized
    static Set<Project> allProjects(Project project) {
        final thisLot = project.subprojects
        final children = thisLot.collectMany { allProjects(it) }.toSet() as Set<Project>
        (thisLot + children) as Set<Project>
    }

    /**
     * Adds ivypot system properties to all Test tasks.
     *
     * @param project Associated project
     * @param repoDetails Instance of a {@link OfflineRepositoryConfiguration} within the associated project.
     */
    static void addTestSysPropProvider(
        Project project,
        OfflineRepositoryReadOnlyConfiguration repoDetails
    ) {
        final clap = new CommandLineArgumentProvider() {
            @Override
            Iterable<String> asArguments() {
                repoDetails.asSystemProperties.get().collect {
                    "-D${it.key}=${it.value}".toString()
                }
            }
        }

        project.tasks.withType(Test).configureEach { task ->
            task.jvmArgumentProviders.add(clap)
        }
    }

    /**
     * Declares the correct outgoing configuration for publishing Ivypot metadata.
     *
     * @param project Associated project
     * @param task Task generating the metadata file.
     */
    static void declareOutgoingMetadataConfiguration(Project project, TaskProvider<?> task) {
        ProjectOperations.find(project).configurations.createSingleOutgoingConfiguration(
            METADATA_CONFIG_OUTGOING,
            false
        ) { attrs ->
            attrs.attribute(
                Attribute.of(IVYPOT_ATTRIBUTE_NAME, String),
                IVYPOT_ATTRIBUTE_VALUE
            )
        }

        project.configurations.getByName(METADATA_CONFIG_OUTGOING).outgoing { cp ->
            cp.artifact(task)
        }
    }

    /**
     * Declares the correct outgoing configuration for publishing Ivypot repo details.
     *
     * @param project Associated project
     * @param task Task generating the metadata file.
     */
    static void declareOutgoingRepoDetailsConfiguration(Project project, TaskProvider<?> task) {
        ProjectOperations.find(project).configurations.createSingleOutgoingConfiguration(
            REPODETAILS_CONFIG_OUTGOING,
            false
        ) { attrs ->
            attrs.attribute(
                Attribute.of(IVYPOT_ATTRIBUTE_NAME, String),
                IVYPOT_ATTRIBUTE_VALUE
            )
        }

        project.configurations.getByName(REPODETAILS_CONFIG_OUTGOING).outgoing { cp ->
            cp.artifact(task)
        }
    }

    private final static String REPO_NAME = 'IvyPotOfflineRepo'
}
