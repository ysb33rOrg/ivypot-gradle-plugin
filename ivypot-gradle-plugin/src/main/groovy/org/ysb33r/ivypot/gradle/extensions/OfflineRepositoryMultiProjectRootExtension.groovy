/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.extensions

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.specs.Spec
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.ivypot.gradle.CredentialsHandler
import org.ysb33r.ivypot.gradle.internal.extensions.DefaultCredentialsHandler
import org.ysb33r.ivypot.gradle.internal.utils.SyncUtils

/**
 * The extension that is applied to the root project in a multi-project setup.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OfflineRepositoryMultiProjectRootExtension extends OfflineRepositoryBase
    implements HasOfflineRepositoryConfiguration, HasBinaryRepositories, HasCredentials {

    OfflineRepositoryMultiProjectRootExtension(Project project) {
        super(project)
        this.projectPaths = SyncUtils.allProjects(project)*.path.asImmutable()
        this.dependencyHandler = project.dependencies
        this.creds = new DefaultCredentialsHandler(project)
    }

    @Override
    CredentialsHandler getCredentials() {
        this.creds
    }

    @Override
    void credentials(Action<CredentialsHandler> configurator) {
        configurator.execute(this.creds)
    }

    @Override
    void credentials(@DelegatesTo(CredentialsHandler) Closure<?> configurator) {
        ClosureUtils.configureItem(this.creds, configurator)
    }

    /**
     * Selects which subprojects have to be included.
     *
     * This must be called in the root project otherwise no subproject will be considered for Ivypot metadata.
     *
     * @param predicate The predicate is passed a project path and should return boolean to indicate whether the
     *  subproject should be queried for Ivypot metadata.
     */
    void projectsWithDependencies(Spec<String> predicate) {
        addIncomingDocumentArtifacts(this.projectPaths.findAll { it != ':' && predicate.isSatisfiedBy(it) })
    }

    private void addIncomingDocumentArtifacts(Collection<String> projectPaths) {
        projectPaths.each { projectPath ->
            dependencyHandler.add(
                SyncUtils.METADATA_CONFIG_INCOMING,
                dependencyHandler.project(path: projectPath, configuration: SyncUtils.METADATA_CONFIG_OUTGOING)
            )
        }
    }

    private final List<String> projectPaths
    private final DependencyHandler dependencyHandler
    private final CredentialsHandler creds
}
