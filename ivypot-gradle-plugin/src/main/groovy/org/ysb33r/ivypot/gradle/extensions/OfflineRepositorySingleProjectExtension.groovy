/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.extensions

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.ivypot.gradle.CacheBinaries
import org.ysb33r.ivypot.gradle.ConfigurationSelector
import org.ysb33r.ivypot.gradle.CredentialsHandler
import org.ysb33r.ivypot.gradle.ExternalArtifactRepositories
import org.ysb33r.ivypot.gradle.internal.extensions.DefaultArtifactRepositories
import org.ysb33r.ivypot.gradle.internal.extensions.DefaultCacheBinaries
import org.ysb33r.ivypot.gradle.internal.extensions.DefaultConfigurationSelector
import org.ysb33r.ivypot.gradle.internal.extensions.DefaultCredentialsHandler

/**
 * The extension that is applied to a single project setup.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OfflineRepositorySingleProjectExtension extends OfflineRepositoryBase
    implements HasOfflineRepositoryConfiguration, HasBinaryRepositories, HasConfigurationSelector, HasCacheBinaries,
        HasExternalArtifactRepositories, HasCredentials {

    OfflineRepositorySingleProjectExtension(Project project) {
        super(project)
        this.selector = new DefaultConfigurationSelector(project)
        this.binaries = new DefaultCacheBinaries(project)
        this.artifactRepositories = new DefaultArtifactRepositories(project)
        this.creds = new DefaultCredentialsHandler(project)
    }

    /**
     * Access to the configuration selector.
     *
     * @return An instance that implements {@link ConfigurationSelector}.
     */
    @Override
    ConfigurationSelector getConfigurations() {
        this.selector
    }

    @Override
    void configurations(Action<ConfigurationSelector> configurator) {
        configurator.execute(this.selector)
    }

    @Override
    void configurations(@DelegatesTo(ConfigurationSelector) Closure<?> configurator) {
        ClosureUtils.configureItem(this.selector, configurator)
    }

    @Override
    ExternalArtifactRepositories getRepositories() {
        this.artifactRepositories
    }

    @Override
    void repositories(Action<ExternalArtifactRepositories> configurator) {
        configurator.execute(this.artifactRepositories)
    }

    @Override
    void repositories(@DelegatesTo(ExternalArtifactRepositories) Closure<?> configurator) {
        ClosureUtils.configureItem(this.artifactRepositories, configurator)
    }

    /**
     * Access to an implementation of {@link CacheBinaries}
     * @return Instance
     */
    @Override
    CacheBinaries getCacheBinaries() {
        this.binaries
    }

    /**
     * Configures binaries to be added.
     *
     * @param configurator Configurating action.
     */
    @Override
    void cacheBinaries(Action<CacheBinaries> configurator) {
        configurator.execute(this.binaries)
    }

    /**
     * Configures binaries to be added.
     *
     * @param configurator Configurating closure.
     */
    @Override
    void cacheBinaries(@DelegatesTo(CacheBinaries) Closure<?> configurator) {
        ClosureUtils.configureItem(this.binaries, configurator)
    }

    @Override
    CredentialsHandler getCredentials() {
        this.creds
    }

    @Override
    void credentials(Action<CredentialsHandler> configurator) {
        configurator.execute(this.creds)
    }

    @Override
    void credentials(@DelegatesTo(CredentialsHandler) Closure<?> configurator) {
        ClosureUtils.configureItem(this.creds, configurator)
    }

    private final ConfigurationSelector selector
    private final CacheBinaries binaries
    private final ExternalArtifactRepositories artifactRepositories
    private final CredentialsHandler creds
}
