/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.extensions;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.ysb33r.ivypot.gradle.CacheBinaries;

/**
 * Whether an extension can record arbitrary binaries to be cached.
 * <p>
 *     Binaries will only be cached if the group name matches that of binary repositories.
 * </p>
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface HasCacheBinaries {

    /**
     * Access to an implementation of {@link CacheBinaries}
     * @return Instance
     */
    CacheBinaries getCacheBinaries();

    /**
     * Configures binaries to be added.
     *
     * @param configurator Configurating action.
     */
    void cacheBinaries(Action<CacheBinaries> configurator);

    /**
     * Configures binaries to be added.
     *
     * @param configurator Configurating closure.
     */
    void cacheBinaries(@DelegatesTo(CacheBinaries.class)Closure<?> configurator);
}
