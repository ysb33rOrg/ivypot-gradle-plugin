/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

import java.time.LocalDateTime

import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.REPODETAILS_FILENAME

/**
 * Generates repository details to a file.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OfflineRepositoryRepoDetailsGenerator extends GrolifantDefaultTask {

    OfflineRepositoryRepoDetailsGenerator() {
        this.outputFile = project.objects.property(File)
        this.location = project.objects.property(String)

        inputs.property('location', this.location)
        outputs.file(this.outputFile)
    }

    void setLocation(Provider<File> location) {
        this.location.set(location.map { it.absolutePath })
        this.outputFile.set(location.map { new File(it, REPODETAILS_FILENAME) })
    }

    @TaskAction
    void exec() {
        final props = new Properties()
        props['repo'] = location.get()
        outputFile.get().withWriter { w -> props.store(w, LocalDateTime.now().toString()) }
    }

    private final Property<File> outputFile
    private final Property<String> location
}
