/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle

import groovy.transform.CompileStatic
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask
import org.ysb33r.ivypot.gradle.internal.data.SubprojectMetadata
import org.ysb33r.ivypot.gradle.repositories.RepositoryMetadata
import org.ysb33r.ivypot.remote.BinaryDependency
import org.ysb33r.ivypot.remote.IvyDependency

/**
 * Generates off-line metadata in a subproject.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OfflineRepositoryMetadata extends GrolifantDefaultTask {

    OfflineRepositoryMetadata() {
        this.output = fsOperations().buildDirDescendant('ivypot/metadata/metadata.bin')
        this.dependencySet = project.objects.listProperty(IvyDependency)
        this.binaries = project.objects.listProperty(BinaryDependency)
        this.repositories = project.objects.mapProperty(String, RepositoryMetadata)

        inputs.property('dependencies', dependencySet)
        inputs.property('binaries', binaries)
        inputs.property('repositories', repositories)
    }

    @OutputFile
    Provider<File> getOutputFile() {
        this.output
    }

    void setDependencies(Provider<List<IvyDependency>> data) {
        this.dependencySet.set(data)
    }

    void setRepositoryData(Provider<Map<String, RepositoryMetadata>> data) {
        this.repositories.set(data)
    }

    void setBinaryDependencies(Provider<List<BinaryDependency>> data) {
        this.binaries.set(data)
    }

    @TaskAction
    void exec() {
        final data = new SubprojectMetadata(
            repositories: new TreeMap<String, RepositoryMetadata>(),
            binaries: [],
            dependencies: []
        )
        data.repositories.putAll(this.repositories.get())
        data.binaries.addAll(this.binaries.get())
        data.dependencies.addAll(this.dependencySet.get())
        SubprojectMetadata.serializeData(outputFile.get(), data)
    }

    private final ListProperty<IvyDependency> dependencySet
    private final ListProperty<BinaryDependency> binaries
    private final MapProperty<String, RepositoryMetadata> repositories
    private final Provider<File> output
}
