/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle

import groovy.transform.CompileStatic
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.PathSensitivity
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.util.PatternFilterable
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask
import org.ysb33r.ivypot.gradle.internal.utils.SyncUtils

import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.REPODETAILS_CONFIG_RESOLVE
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.REPODETAILS_FILENAME

/**
 * Generates repository details to a file.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OfflineRepositoryRepoConsumer extends GrolifantDefaultTask {

    OfflineRepositoryRepoConsumer() {
        this.outputFile = project.objects.property(File)
        this.outputFile.set(fsOperations().buildDirDescendant(".repo/${REPODETAILS_FILENAME}"))

        this.details = project.configurations.getByName(REPODETAILS_CONFIG_RESOLVE)
            .asFileTree.matching { PatternFilterable p ->
            p.include(REPODETAILS_FILENAME)
        }
    }

    @InputFiles
    @PathSensitive(PathSensitivity.RELATIVE)
    FileCollection getInputFiles() {
        this.details
    }

    @OutputFile
    Provider<File> getRepoLocation() {
        this.outputFile
    }

    @TaskAction
    void exec() {
        fsOperations().copy {
            it.from(details)
            it.into(outputFile.get().parentFile)
            it.include("**/${SyncUtils.REPODETAILS_FILENAME}")
        }
    }

    private final Property<File> outputFile
    private final FileCollection details
}
