/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.utils

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.ArtifactRepository
import org.gradle.api.artifacts.repositories.IvyArtifactRepository
import org.gradle.api.artifacts.repositories.IvyPatternRepositoryLayout
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.ysb33r.ivypot.gradle.internal.repositories.IvyMetadata
import org.ysb33r.ivypot.gradle.repositories.RepositoryMetadata
import org.ysb33r.ivypot.gradle.repositories.RepositoryTypes

import static org.gradle.api.artifacts.repositories.IvyArtifactRepository.IVY_ARTIFACT_PATTERN

/**
 * Utilities for dealing with artifact repositories
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
@CompileStatic
@Slf4j
class RepositoryUtils {
    public static final String IVY_URLS = 'ivyUrls'
    public static final String ARTIFACT_URLS = 'artifactUrls'

    /**
     * Creates a clickable file URI.
     *
     * @param file File to urize
     * @return String version of file URI.
     */
    static String fileUri(File file) {
        final uri = file.absoluteFile.toURI().toString()
        if (uri.startsWith(SHORT_FILE_SCHEMA_PREFIX) && !uri.startsWith(FULL_FILE_SCHEMA_PREFIX)) {
            uri.replaceFirst(SHORT_FILE_SCHEMA_PREFIX, FULL_FILE_SCHEMA_PREFIX)
        } else {
            uri
        }
    }

    static Map<String, RepositoryMetadata> select(RepositoryHandler repositories,
                                                  Collection<String> excluded,
                                                  Map<String, IvyMetadata> ivyInfo) {
        repositories.findAll { ArtifactRepository ar ->
            (ar instanceof IvyArtifactRepository || ar instanceof MavenArtifactRepository) && !(ar.name in excluded)
        }.collect { ar ->
            switch (ar) {
                case IvyArtifactRepository:
                    return fromIvy((IvyArtifactRepository) ar, ivyInfo)
                case MavenArtifactRepository:
                    return fromMaven((MavenArtifactRepository) ar)
                default:
                    return null
            }
        }.findAll { it }
            .collectEntries { [it.repositoryProperties.name, it] }
    }

    private static RepositoryMetadata fromIvy(IvyArtifactRepository ivy, Map<String, IvyMetadata> info) {
        final metadata = info[ivy.name]
        final ivyPatterns = metadata == null ? [IVY_ARTIFACT_PATTERN] :
            (metadata.artifactOnly ? [] : (metadata.ivyPatterns.empty ? [IVY_ARTIFACT_PATTERN] : metadata.ivyPatterns))
        final artifactPatterns = metadata == null ? [IVY_ARTIFACT_PATTERN] :
            (metadata.artifactPatterns.empty ? [IVY_ARTIFACT_PATTERN] : metadata.artifactPatterns)

        new RepositoryMetadata().tap {
            it.url = ivy.url.toURL()
            it.type = RepositoryTypes.IVY
            it.repositoryProperties.putAll([
                name           : ivy.name,
                (IVY_URLS)     : ivyPatterns.collect { "${ivy.url}/${it}".toString() },
                (ARTIFACT_URLS): artifactPatterns.collect { "${ivy.url}/${it}".toString() }
            ])

            ivy.patternLayout { IvyPatternRepositoryLayout pat -> it.m2compatible = pat.m2Compatible
            }
        }
    }

    private static RepositoryMetadata fromMaven(MavenArtifactRepository mvn) {
        new RepositoryMetadata().tap {
            it.url = mvn.url.toURL()
            it.type = mvn.artifactUrls.empty ? RepositoryTypes.MAVEN : RepositoryTypes.MAVEN_WITH_EXTRA_URLS
            it.m2compatible = true
            it.repositoryProperties.putAll([
                name           : mvn.name,
                (ARTIFACT_URLS): mvn.artifactUrls
            ])
        }
    }

    private static final String SHORT_FILE_SCHEMA_PREFIX = 'file:/'
    private static final String FULL_FILE_SCHEMA_PREFIX = 'file:///'
}
