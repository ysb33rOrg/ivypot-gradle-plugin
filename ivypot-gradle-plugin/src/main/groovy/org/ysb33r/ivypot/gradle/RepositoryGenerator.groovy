/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask
import org.ysb33r.ivypot.gradle.internal.utils.SyncUtils

/**
 * Generate Gradle DSL files that can be included in tests or build scripts.
 * One Groovy DSL and one Kotlin DSL file will be generated.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@CacheableTask
class RepositoryGenerator extends GrolifantDefaultTask {

    RepositoryGenerator() {
        this.groovyFile = project.objects.property(File)
        this.kotlinFile = project.objects.property(File)
        this.uri = project.objects.property(URI)

        inputs.property('uri', this.uri)
        outputs.file(this.groovyFile)
        outputs.file(this.kotlinFile)
    }

    void setRepositoryUri(Provider<URI> uri) {
        this.uri.set(uri)
    }

    void setGroovyRepository(Provider<File> gfile) {
        this.groovyFile.set(gfile)
    }

    void setKotlinRepository(Provider<File> kfile) {
        this.kotlinFile.set(kfile)
    }

    @TaskAction
    void exec() {
        SyncUtils.writeRepositoryFiles(
            this.uri,
            this.groovyFile,
            this.kotlinFile
        )
    }

    private final Property<File> groovyFile
    private final Property<File> kotlinFile
    private final Property<URI> uri
}
