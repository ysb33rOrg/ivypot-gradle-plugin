/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.extensions

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.ModuleDependency
import org.gradle.api.artifacts.ResolvedArtifact
import org.gradle.api.provider.Provider
import org.gradle.api.specs.Spec
import org.ysb33r.ivypot.gradle.ConfigurationSelector
import org.ysb33r.ivypot.gradle.dependencies.ResolvedDependency
import org.ysb33r.ivypot.remote.IvyDependency

/**
 * Implementation of a configuration selector.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultConfigurationSelector implements ConfigurationSelector {

    DefaultConfigurationSelector(Project project) {
        this.configurationContainer = project.configurations

        this.dependencyProvider = project.provider { ->
            owner.extractDependencies()
        }

        this.dependencyMapProvider = this.dependencyProvider.map { owner.extractDependenciesAsMaps(it) }
    }

    /**
     * A provider to a set of dependencies represented as maps instead.
     *
     * @return Provider to a set of mapped dependencies.
     */
    @Override
    Provider<List<IvyDependency>> getMappedDependencies() {
        this.dependencyMapProvider
    }

    /**
     * A provider to a set of dependencies that will be resolved from allowed configurations.
     *
     * @return Provider to a set of dependencies that must be sync'd to the offline repository.
     */
    @Override
    Provider<List<Dependency>> getDependencies() {
        this.dependencyProvider
    }

    /**
     * Include only configuration that are in the named list.
     *
     * <p>
     *     Can be called more than once.
     * </p>
     *
     * @param configurationNames Names of configurations.
     */
    @Override
    void named(String... configurationNames) {
        this.configurationNames.addAll(configurationNames)
    }

    /**
     * Sets a filter that can further exclude any configurations.
     *
     * <p>
     *     This filter is applied only after any named configurations are included.
     * </p>
     * @param spec
     */
    @Override
    void filter(Spec<? extends Configuration> spec) {
        this.filter = spec
    }

    @SuppressWarnings(['UnusedPrivateMethod', 'UnnecessaryCast'])
    private List<IvyDependency> extractDependenciesAsMaps(Collection<Dependency> deps) {
        deps.findAll { !it.version.endsWith('SNAPSHOT') }
            .collect { dep ->
                if (dep instanceof ModuleDependency) {
                    ModuleDependency modDep = (ModuleDependency) dep
                    new IvyDependency([
                        organisation: dep.group,
                        module      : dep.name,
                        revision    : dep.version,
                        transitive  : modDep.transitive,
                        typeFilter  : modDep.artifacts?.getAt(0)?.type ?: WILDCARD,
                        confFilter  : WILDCARD,
                        classifier  : modDep.artifacts?.getAt(0)?.classifier,
                        extension   : modDep.artifacts?.getAt(0)?.extension
                    ])
                } else if (dep instanceof ResolvedDependency) {
                    Map<String, String> rdMap = ((ResolvedDependency) dep).asMap()
                    new IvyDependency(rdMap)
                } else {
                    new IvyDependency([
                        organisation: dep.group,
                        module      : dep.name,
                        revision    : dep.version,
                        transitive  : true,
                        typeFilter  : WILDCARD,
                        confFilter  : WILDCARD
                    ] as Map<? extends String, ? extends String>)
                }
            }
    }

    private Set<Configuration> allowedConfigurations() {
        final resolvables = configurationContainer.findAll { Configuration it -> it.canBeResolved }
        final onNameBasis = configurationNames.empty ? resolvables :
            resolvables.findAll { it.name in configurationNames }
        filter ? onNameBasis.findAll { filter.isSatisfiedBy(it) } : onNameBasis
    }

    @SuppressWarnings('UnusedPrivateMethod')
    private List<Dependency> extractDependencies() {
        final List<Dependency> deps = []

        allowedConfigurations().each { Configuration cfg ->
            Iterable<Dependency> externalModules = getExternalModuleDependencies(cfg)

            if (externalModules.any { Dependency dep -> !dep.version }) {
                Configuration temp = cfg.copy { Dependency dep ->
                    !dep.version
                }
                temp.canBeResolved = true
                Set<Dependency> transitiveDependencies = temp.dependencies.findAll { Dependency it ->
                    ExternalModuleDependency dep = (ExternalModuleDependency) it
                    dep.transitive
                }
                for (ResolvedArtifact resolved : temp.resolvedConfiguration.resolvedArtifacts) {
                    boolean isTransitive = transitiveDependencies.any { t ->
                        t.group == resolved.moduleVersion.id.group &&
                            t.name == resolved.moduleVersion.id.name
                    }
                    deps.add(new ResolvedDependency(resolved, isTransitive))
                }
            } else {
                deps.addAll(externalModules)
            }
            deps
        }

        deps
    }

    private Iterable<Dependency> getExternalModuleDependencies(final Configuration configuration) {
        configuration.allDependencies.findAll { Dependency dep ->
            dep instanceof ExternalModuleDependency
        }
    }

    private final Set<String> configurationNames = []
    private final ConfigurationContainer configurationContainer
    private final Provider<List<Dependency>> dependencyProvider
    private final Provider<List<IvyDependency>> dependencyMapProvider
    private Spec<? extends Configuration> filter

    private static final String WILDCARD = '*'
}
