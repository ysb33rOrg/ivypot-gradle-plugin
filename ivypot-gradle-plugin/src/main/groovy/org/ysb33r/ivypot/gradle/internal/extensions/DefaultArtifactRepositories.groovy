/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.extensions

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.ArtifactRepositoryContainer
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.ivypot.gradle.ExternalArtifactRepositories
import org.ysb33r.ivypot.gradle.internal.repositories.IvyMetadata
import org.ysb33r.ivypot.gradle.internal.utils.RepositoryUtils
import org.ysb33r.ivypot.gradle.repositories.IvyAdditionalInfo
import org.ysb33r.ivypot.gradle.repositories.RepositoryMetadata

/**
 * Default repository declaration.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultArtifactRepositories implements ExternalArtifactRepositories {

    DefaultArtifactRepositories(Project project) {
        this.repositories = project.repositories
        this.excluded = [ArtifactRepositoryContainer.DEFAULT_MAVEN_LOCAL_REPO_NAME].toSet()
        this.ivyMetadataMap = [:]
        this.repoProvider = project.provider { ->
            RepositoryUtils.select(
                owner.repositories as RepositoryHandler,
                owner.excluded,
                owner.ivyMetadataMap
            )
        }
    }

    @Override
    Provider<Map<String, RepositoryMetadata>> getAllowedRepositoryMetadata() {
        this.repoProvider
    }

    /**
     * Exclude repositories by name.
     *
     * <p>
     *     Certain repositories might not be suitable for obtaining artifacts for offline caching.
     * </p>
     *
     * @param names Names of repositories to exclude
     */
    @Override
    void excludeByName(String... names) {
        excluded.addAll(names)
    }

    /**
     * Additional Ivy & Artifact patterns for a given Ivy repository.
     *
     * <p>This is a workaround for the problem that is it difficult to extract this information from the Gradle API.</p>
     *
     * @param name Name of Ivy repository
     * @param configurator Info configurator
     */
    @Override
    void forIvy(String name, Action<IvyAdditionalInfo> configurator) {
        if (!ivyMetadataMap.containsKey(name)) {
            ivyMetadataMap[name] = new IvyMetadata()
        }
        configurator.execute(ivyMetadataMap[name])
    }

    /**
     * Additional Ivy & Artifact patterns for a given Ivy repository.
     *
     * <p>This is a workaround for the problem that is it difficult to extract this information from the Gradle API.</p>
     *
     * @param name Name of Ivy repository
     * @param configurator Info configurator
     */
    @Override
    void forIvy(String name, @DelegatesTo(IvyAdditionalInfo) Closure<?> configurator) {
        if (!ivyMetadataMap.containsKey(name)) {
            ivyMetadataMap[name] = new IvyMetadata()
        }
        ClosureUtils.configureItem(ivyMetadataMap[name], configurator)
    }

    private final RepositoryHandler repositories
    private final Set<String> excluded
    private final Provider<Map<String, RepositoryMetadata>> repoProvider
    private final Map<String, IvyMetadata> ivyMetadataMap
}
