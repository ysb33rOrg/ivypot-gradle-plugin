/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.extensions

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.ivypot.gradle.BinaryRepositoryDescriptor
import org.ysb33r.ivypot.gradle.BinaryRepositorySourceSpec
import org.ysb33r.ivypot.gradle.BinaryRepositorySpec
import org.ysb33r.ivypot.gradle.OfflineRepositoryConfiguration
import org.ysb33r.ivypot.gradle.internal.extensions.DefaultRepositoryConfiguration
import org.ysb33r.ivypot.gradle.internal.repositories.DefaultBinaryRepository

import java.util.concurrent.Callable

/**
 * A base class to for root and single project extensions.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
class OfflineRepositoryBase implements HasOfflineRepositoryConfiguration, HasBinaryRepositories {

    @Override
    OfflineRepositoryConfiguration getRepoDetails() {
        this.repo
    }

    @Override
    void repoDetails(Action<OfflineRepositoryConfiguration> configurator) {
        configurator.execute(this.repo)
    }

    @Override
    void repoDetails(@DelegatesTo(OfflineRepositoryConfiguration) Closure<?> configurator) {
        ClosureUtils.configureItem(this.repo, configurator)
    }

    @Override
    Provider<Map<String, BinaryRepositoryDescriptor>> getBinaryRepositories() {
        this.repoProvider
    }

    /**
     * Configures binary repositories.
     *
     * @param action configurator
     */
    @Override
    void binaryRepositories(Action<? super BinaryRepositorySourceSpec> action) {
        action.execute(this.configureSources)
    }

    /**
     * Configures binary repositories.
     *
     * @param configurator Configurator
     */
    @Override
    void binaryRepositories(@DelegatesTo(BinaryRepositorySourceSpec) Closure<?> configurator) {
        ClosureUtils.configureItem(this.configureSources, configurator)
    }

    protected OfflineRepositoryBase(Project project) {
        this.repo = new DefaultRepositoryConfiguration(ConfigCacheSafeOperations.from(project))
        this.repositories = [:]
        this.configureSources = new SourceSpec(project.objects, this.repositories)
        this.repoProvider = project.provider({ r ->
            r.collectEntries { k, v -> [k, (BinaryRepositoryDescriptor) v] }
        }.curry(this.repositories) as Callable<Map<String, BinaryRepositoryDescriptor>>)
    }

    private final DefaultRepositoryConfiguration repo
    private final Map<String, DefaultBinaryRepository> repositories
    private final Provider<Map<String, BinaryRepositoryDescriptor>> repoProvider
    private final SourceSpec configureSources

    private static class SourceSpec implements BinaryRepositorySourceSpec {
        SourceSpec(ObjectFactory objectFactory, Map<String, DefaultBinaryRepository> repositoryMap) {
            this.repositoryMap = repositoryMap
            this.objectFactory = objectFactory
        }

        /**
         * Configures a repository. If it does not exist, it is added.
         *
         * @param name Name of repository.
         * @param configurator Configurator.
         */
        @Override
        void source(String name, Action<? super BinaryRepositorySpec> configurator) {
            if (!this.repositoryMap.containsKey(name)) {
                this.repositoryMap.put(name, objectFactory.newInstance(DefaultBinaryRepository, name))
            }

            configurator.execute(this.repositoryMap[name])
        }

        /**
         * Configures a repository. If it does not exist, it is added.
         *
         * @param name Name of repository.
         * @param configurator Configurator.
         */
        @Override
        void source(String name, @DelegatesTo(BinaryRepositorySpec) Closure<?> configurator) {
            if (!this.repositoryMap.containsKey(name)) {
                this.repositoryMap.put(name, objectFactory.newInstance(DefaultBinaryRepository, name))
            }
            ClosureUtils.configureItem(this.repositoryMap[name], configurator)
        }

        private final Map<String, DefaultBinaryRepository> repositoryMap
        private final ObjectFactory objectFactory
    }
}
