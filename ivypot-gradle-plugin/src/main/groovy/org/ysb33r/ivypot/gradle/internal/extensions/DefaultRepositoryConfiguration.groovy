/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.extensions

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.ivypot.gradle.OfflineRepositoryConfiguration

/**
 * Configures the details of an offline repository
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultRepositoryConfiguration extends AbstractRepositoryConfiguration implements OfflineRepositoryConfiguration {

    DefaultRepositoryConfiguration(ConfigCacheSafeOperations ccso) {
        super(ccso)
        this.subpath = ccso.providerTools().property(String)
        this.subpath.set('.offline-repo')
        updateRoot(ccso.fsOperations().provideRootDir().zip(this.subpath) { file, path ->
            new File(file, path)
        })
    }

    /**
     * The location relative to the root project where the offline repository is located.
     *
     * <p>If not configured, it defaults to {@code .offline-repo}.</p>
     *
     * @param location Relative path. Anything convertible to a string using
     * {@link org.ysb33r.grolifant5.api.core.StringTools#stringize}.
     */
    @Override
    void setRoot(Object location) {
        stringTools.updateStringProperty(subpath, location)
    }

    private final Property<String> subpath
}
