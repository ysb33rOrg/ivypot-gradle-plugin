/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.provider.Provider;

import java.util.Map;

/**
 * Handles credentials.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface CredentialsHandler {

    void forName(String name, Action<CredentialsConfigurator> configurator);

    void forName(String name, @DelegatesTo(CredentialsConfigurator.class) Closure<?> configurator);

    Provider<Map<String, CredentialProviders>> getCredentialMetadata();
}
