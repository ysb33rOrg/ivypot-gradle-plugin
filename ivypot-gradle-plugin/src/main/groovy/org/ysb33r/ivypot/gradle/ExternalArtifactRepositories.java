/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.provider.Provider;
import org.ysb33r.ivypot.gradle.repositories.IvyAdditionalInfo;
import org.ysb33r.ivypot.gradle.repositories.RepositoryMetadata;

import java.util.Map;

/**
 * Defines an artifact repository that is typically derived from Maven or Ivy.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface ExternalArtifactRepositories {

    /**
     * Obtain metadata on repositories that can be used.
     *
     * @return Metadata on repositories
     */
    Provider<Map<String, RepositoryMetadata>> getAllowedRepositoryMetadata();

    /**
     * Exclude repositories by name.
     *
     * <p>
     * Certain repositories might not be suitable for obtaining artifacts for offline caching.
     * </p>
     *
     * @param names Names of repositories to exclude
     */
    void excludeByName(String... names);

    /**
     * Additional Ivy and Artifact patterns for a given Ivy repository.
     *
     * <p>This is a workaround for the problem that is it difficult to extract this information from the Gradle API.</p>
     *
     * @param name Name of Ivy repository
     * @param configurator Info configurator
     */
    void forIvy(String name, Action<IvyAdditionalInfo> configurator);

    /**
     * Additional Ivy and Artifact patterns for a given Ivy repository.
     *
     * <p>This is a workaround for the problem that is it difficult to extract this information from the Gradle API.</p>
     *
     * @param name Name of Ivy repository
     * @param configurator Info configurator
     */
    void forIvy(String name, @DelegatesTo(IvyAdditionalInfo.class) Closure<?> configurator);
}
