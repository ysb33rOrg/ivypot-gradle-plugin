/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle;

import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.Dependency;
import org.gradle.api.provider.Provider;
import org.gradle.api.specs.Spec;
import org.ysb33r.ivypot.remote.IvyDependency;

import java.util.List;
import java.util.Set;

/**
 * Includes / excludes configuration by name or other criteria.
 *
 * <p>
 *     Only configurations that are resolvable are considered before any filters are applied.
 *     If no filters are configured, then all resolvable cnofigurations will be searched.
 * </p>
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface ConfigurationSelector {

    /**
     * A provider to a set of dependencies that will be resolved from allowed configurations.
     *
     * When the provider is accessed all dependencies in allowed  configurations will be resolved.
     *
     * @return Provider to a set of dependencies that must be sync'd to the offline repository.
     */
    Provider<List<Dependency>> getDependencies();

    /**
     * A provider to a set of dependencies represented as maps instead.
     *
     * @return Provider to a set of maps.
     */
    Provider<List<IvyDependency>> getMappedDependencies();

    /**
     * Include only configuration that are in the named list.
     *
     * <p>
     *     Can be called more than once.
     * </p>
     *
     * @param configurationNames Names of configurations.
     */
    void named(String... configurationNames);

    /**
     * Sets a filter that can further exclude any configurations.
     *
     * <p>
     *     This filter is applied only after any named configurations are included.
     * </p>
     * @param spec A predicate that is passed a {@link Configuration} and which should
     *             return {@code true} if the configuration should be included.
     */
    void filter(Spec<? extends Configuration> spec);
}
