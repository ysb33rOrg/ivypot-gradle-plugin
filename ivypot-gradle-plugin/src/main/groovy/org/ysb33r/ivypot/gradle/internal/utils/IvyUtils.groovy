/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2020
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//
package org.ysb33r.ivypot.gradle.internal.utils

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.xml.MarkupBuilder
import org.gradle.api.artifacts.repositories.IvyArtifactRepository
import org.ysb33r.ivypot.gradle.CredentialProviders
import org.ysb33r.ivypot.gradle.repositories.RepositoryMetadata
import org.ysb33r.ivypot.gradle.repositories.RepositoryTypes

import static org.ysb33r.ivypot.gradle.internal.utils.RepositoryUtils.ARTIFACT_URLS
import static org.ysb33r.ivypot.gradle.internal.utils.RepositoryUtils.IVY_URLS

/**
 * Utilities for dealing with Ivy.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class IvyUtils {
    public static final String REMOTECHAINNAME = '~~~remote~~~resolvers~~~'

    @CompileDynamic
    @SuppressWarnings('NestedBlockDepth')
    static void writeSettingsFile(
        File settingsFile,
        File repoRoot,
        File cacheDir,
        Map<String, RepositoryMetadata> repositories,
        Iterable<Map<String, String>> repositoryCredentials
    ) {
        settingsFile.parentFile.mkdirs()
        def xmlWriter = new StringWriter()
        def xml = new MarkupBuilder(xmlWriter)
        xml.ivysettings {
            settings(defaultResolver: REMOTECHAINNAME)
            caches(
                defaultCacheDir: repoRoot.absolutePath,
                artifactPattern: IvyArtifactRepository.GRADLE_ARTIFACT_PATTERN,
                ivyPattern: IvyArtifactRepository.GRADLE_ARTIFACT_PATTERN,
                resolutionCacheDir: cacheDir.absolutePath
            )

            repositoryCredentials.each { repo ->
                credentials(repo)
            }
            resolvers {
                chain(name: REMOTECHAINNAME, returnFirst: true) {
                    repositories.each { repo ->
                        switch (repo.value.type) {
                            case RepositoryTypes.MAVEN:
                                ibiblio(name: repo.key, m2compatible: true, root: repo.value.url)
                                break
                            case RepositoryTypes.IVY:
                                url(
                                    name: repo.key,
                                    m2compatible: repo.value.m2compatible
                                ) {
                                    repo.value.repositoryProperties[IVY_URLS].each { pat ->
                                        ivy(pattern: pat)
                                    }
                                    repo.value.repositoryProperties[ARTIFACT_URLS].each { pat ->
                                        artifact(pattern: pat)
                                    }
                                }
                                break
                        }
                    }
                }
            }
        }
        settingsFile.text = xmlWriter.toString()
    }

    /**
     * Extract credentials and map them to repositories.
     *
     * @param credMap Map of credentials
     * @param repoMap Map of repositories
     * @return Credentials mapped by property names, property value and host names and ports.
     */
    static List<Map<String, String>> extractCredentialsMappings(
        Map<String, CredentialProviders> credMap,
        Map<String, RepositoryMetadata> repoMap
    ) {
        credMap.findAll { repoMap.containsKey(it.key) }
            .collect { k, v ->
                final url = repoMap[k].url
                final usernameProp = "credential.${toSafePropertyName(k)}.username".toString()
                final passwordProp = "credential.${toSafePropertyName(k)}.password".toString()
                final userpass = [
                    usernameProp : usernameProp,
                    passwordProp : passwordProp,
                    usernameValue: v.username.get(),
                    passwordValue: v.password.get(),
                    host         : url.host,
                    port         : url.port.toString()
                ]
                if (v.realm.present) {
                    userpass['realm'] = v.realm.get()
                }

                userpass as Map<String, String>
            }
    }

    /**
     * Creates a propery name that can be used both as a system property and an ANT property.
     *
     * @param prop Original property
     * @return Safe property name
     */
    static String toSafePropertyName(String prop) {
        prop.toLowerCase(Locale.US).replaceAll(~/\s+/, '_')
    }
}
