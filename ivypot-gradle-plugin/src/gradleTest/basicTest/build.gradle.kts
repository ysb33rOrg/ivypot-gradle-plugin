plugins {
    id("org.ysb33r.ivypot")
}

// tag::kotlin-example[]
val simple by configurations.creating
val github by configurations.creating

repositories {
    mavenCentral()
}

dependencies {
    simple("com.slack.api:slack-app-backend:1.8.0")
    github(":ivypot-gradle-plugin:master@zip")
}

ivypot {
    repoDetails {
        setRoot(".repo")
    }

    binaryRepositories {
        source("nodejs", {
            setRootUri("https://nodejs.org/dist/")
            setArtifactPattern("v[revision]/[module]-v[revision]-[classifier].[ext]")
        })
    }

    cacheBinaries {
        add("nodejs:node:7.10.0:linux-x64@tar.xz")
    }

    configurations {
        filter { cfg -> cfg.name == "simple" }
    }
}
// end::kotlin-example[]

tasks.register("runGradleTest") {
    dependsOn("syncRemoteRepositories")
}