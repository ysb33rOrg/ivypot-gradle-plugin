/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.utils

import groovy.xml.XmlSlurper
import org.ysb33r.ivypot.gradle.repositories.RepositoryMetadata
import org.ysb33r.ivypot.gradle.repositories.RepositoryTypes
import org.ysb33r.ivypot.gradle.testfixtures.UnitTestSpecification

class IvyUtilsSpec extends UnitTestSpecification {

    File repoRoot
    File ivySettingsFile
    File cacheDir

    void setup() {
        repoRoot = new File(projectDir, '.repo')
        ivySettingsFile = new File(repoRoot, 'ivysettings.xml')
        cacheDir = new File(projectDir, 'tmp/ivypot/.cache')
    }

    void 'Can write a settings file'() {
        setup:
        final repo1 = new RepositoryMetadata().tap {
            url = 'https://repo1.example'.toURL()
            type = RepositoryTypes.MAVEN
            m2compatible: true
        }
        final repo2 = new RepositoryMetadata().tap {
            url = 'https://repo2.example'.toURL()
            type = RepositoryTypes.IVY
            repositoryProperties.putAll([
                artifactUrls: ['https://repo2.example/[organization]'],
                ivyUrls     : ['https://repo2.example/ivy.xml']
            ])
        }
        final repositories = [
            repo1: repo1,
            repo2: repo2
        ]
        final credentials = [
            [
                host    : 'repo2.example',
                username: '123',
                pasword : '456'
            ]
        ]

        when:
        IvyUtils.writeSettingsFile(
            ivySettingsFile,
            repoRoot,
            cacheDir,
            repositories,
            credentials
        )
        final xmlData = new XmlSlurper().parse(ivySettingsFile)

        then:
        xmlData.resolvers.ibiblio.@name
        xmlData.resolvers.url.@name
    }
}