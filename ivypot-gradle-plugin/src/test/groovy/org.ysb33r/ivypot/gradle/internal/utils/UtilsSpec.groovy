/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.utils

import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.ivypot.gradle.testfixtures.IntegrationSpecification
import spock.lang.Issue

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class UtilsSpec extends IntegrationSpecification {

    @Issue('https://gitlab.com/ysb33rOrg/gradle/ivypot-gradle-plugin/-/issues/60')
    void 'Do not use space assignment when generating repository.gradle'() {
        setup:
        generateRepositoryFile()

        when:
        final result = getGradleRunner(
            IS_GROOVY_DSL,
            [
                'tasks',
                '-i',
                '--warning-mode=all'
            ]
        ).withGradleVersion('8.12').build()

        then:
        result.task(':tasks').outcome == SUCCESS
        !result.output.contains('Space-assignment syntax in Groovy DSL has been deprecated')
    }

    void generateRepositoryFile() {
        final project = ProjectBuilder.builder().build()
        SyncUtils.writeRepositoryFiles(
            project.provider { -> 'http://example.test'.toURI() },
            project.provider { -> buildFile },
            project.provider { -> new File(projectDir, 'ignore') }
        )
    }
}