/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle.internal.extensions

import org.ysb33r.ivypot.gradle.ExternalArtifactRepositories
import org.ysb33r.ivypot.gradle.repositories.RepositoryTypes
import org.ysb33r.ivypot.gradle.testfixtures.UnitTestSpecification

import static org.gradle.api.artifacts.ArtifactRepositoryContainer.DEFAULT_MAVEN_CENTRAL_REPO_NAME

class DefaultArtifactRepositoriesSpec extends UnitTestSpecification {

    public static final String MAVENCENTRAL = DEFAULT_MAVEN_CENTRAL_REPO_NAME
    public static final String GOOGLE = 'Google'

    ExternalArtifactRepositories repositories

    void setup() {
        repositories = new DefaultArtifactRepositories(project)
    }

    void 'Can detect mavenCentral'() {
        setup:
        project.allprojects {
            repositories {
                mavenCentral()
            }
        }

        when:
        final metadata = repositories.allowedRepositoryMetadata.get()

        then:
        metadata.size() == 1
        metadata[MAVENCENTRAL].url
        metadata[MAVENCENTRAL].repositoryProperties.name == MAVENCENTRAL
        metadata[MAVENCENTRAL].type == RepositoryTypes.MAVEN
    }

    void 'Do not include mavenLocal'() {
        setup:
        project.allprojects {
            repositories {
                mavenLocal()
            }
        }

        when:
        final metadata = repositories.allowedRepositoryMetadata.get()

        then:
        metadata.isEmpty()
    }

    void 'Can include Maven repo'() {
        setup:
        final repoName = 'myTest'
        final repoUrl = 'https://my-test.example'
        project.allprojects {
            repositories {
                maven {
                    name = repoName
                    url = repoUrl
                }
            }
        }

        when:
        final metadata = repositories.allowedRepositoryMetadata.get()

        then:
        metadata.size() == 1
        metadata[repoName].url == repoUrl.toURL()
        metadata[repoName].type == RepositoryTypes.MAVEN
    }

    void 'Can include Google repository'() {
        setup:
        project.allprojects {
            repositories {
                google()
            }
        }

        when:
        final metadata = repositories.allowedRepositoryMetadata.get()

        then:
        metadata.size() == 1
        metadata[GOOGLE].url
        metadata[GOOGLE].type == RepositoryTypes.MAVEN
    }

    void 'Can include Ivy repository'() {
        setup:
        final repoName = 'myIvyTest'
        final repoUrl = 'https://my-ivy.example'
        project.allprojects {
            repositories {
                ivy {
                    name = repoName
                    url = repoUrl
                }
            }
        }

        when:
        final metadata = repositories.allowedRepositoryMetadata.get()

        then:
        metadata.size() == 1
        metadata[repoName].url == repoUrl.toURL()
        metadata[repoName].type == RepositoryTypes.IVY
    }

    void 'Can include Ivy repository with different layout'() {
        setup:
        final repoName = 'myIvyTest'
        final repoUrl = 'https://my-ivy.example'
        project.allprojects {
            repositories {
                ivy {
                    name = repoName
                    url = repoUrl
//                    layout('gradle')
                    patternLayout {
                        ivy('123')
                        artifact('45')
                    }
                }
            }
        }

        when:
        final metadata = repositories.allowedRepositoryMetadata.get()

        then:
        metadata.size() == 1
        metadata[repoName].url == repoUrl.toURL()
        metadata[repoName].type == RepositoryTypes.IVY
    }
}