/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle

import org.ysb33r.ivypot.gradle.testfixtures.IntegrationSpecification
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE
import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.REPODETAILS_GEN_TASK_NAME
import static org.ysb33r.ivypot.gradle.OfflineRepositoryPlugin.SYNC_TASK_NAME
import static org.ysb33r.ivypot.gradle.internal.utils.SyncUtils.REPODETAILS_FILENAME
import static org.ysb33r.ivypot.remote.BinaryExecutor.BINARY_SUBDIR

class OfflineRepositorySyncSpec extends IntegrationSpecification {

    String subProject = 'sub1'

    void 'Can create an off-line repository for a single project'() {
        setup:
        final repoDir = new File(projectDir, '.offline-repo')
        writeSingleProject()

        when:
        final result = getGradleRunner(true, [SYNC_TASK_NAME, '-s']).build()
        final ivyXml = new File(repoDir, 'ivysettings.xml')

        then:
        result.task(":${SYNC_TASK_NAME}").outcome == SUCCESS
        ivyXml.exists()
        new File(repoDir, 'commons-io').exists()
        new File(repoDir, "${BINARY_SUBDIR}/nodejs/v7.10.0/node-v7.10.0-linux-x64.tar.xz").exists()

        when:
        final result2 = getGradleRunner(true, [SYNC_TASK_NAME, '-s']).build()

        then:
        !result2.output.contains('[SUCCESSFUL ] org.tukaani#xz;1.6!xz.jar')
    }

    void 'Can create an off-line repository for a single project with configuration-cache'() {
        setup:
        final repoDir = new File(projectDir, '.offline-repo')
        writeSingleProject()

        when:
        final result = getGradleRunnerConfigCache(true, [SYNC_TASK_NAME, '-s']).build()
        final ivyXml = new File(repoDir, 'ivysettings.xml')

        then:
        result.task(":${SYNC_TASK_NAME}").outcome == SUCCESS
        ivyXml.exists()
        new File(repoDir, 'commons-io').exists()
        new File(repoDir, "${BINARY_SUBDIR}/nodejs/v7.10.0/node-v7.10.0-linux-x64.tar.xz").exists()
    }

    @Unroll
    void 'Can create an off-line repository for a multi-project #cc'() {
        setup:
        final repoDir = new File(projectDir, '.offline-repo')
        writeMultiProject()

        when:
        final result = (ccMode ?
            getGradleRunnerConfigCache(true, [REPODETAILS_GEN_TASK_NAME, '-s']) :
            getGradleRunner(true, [REPODETAILS_GEN_TASK_NAME, '-s']))
            .build()
        final ivyXml = new File(repoDir, 'ivysettings.xml')

        then:
        result.task(":${SYNC_TASK_NAME}").outcome == SUCCESS
        ivyXml.exists()
        new File(repoDir, 'commons-io').exists()
        new File(repoDir, "${BINARY_SUBDIR}/nodejs/v7.10.0/node-v7.10.0-linux-x64.tar.xz").exists()
        new File(repoDir, REPODETAILS_FILENAME).exists()

        where:
        ccMode | cc
        false  | 'without configuration-cache'
        true   | 'with configuration-cache'
    }

    void 'A subproject can trigger a sync when accessing the location of the repository'() {
        setup:
        writeMultiProject()

        when:
        final result = getGradleRunner(true, ['printRepoLocation', '-s']).build()

        then:
        result.task(":${subProject}:printRepoLocation").outcome == SUCCESS
        result.task(":${REPODETAILS_GEN_TASK_NAME}").outcome == SUCCESS

        when:
        final result2 = getGradleRunner(true, ['printRepoLocation', '-s']).build()

        then:
        result2.task(":${REPODETAILS_GEN_TASK_NAME}").outcome == UP_TO_DATE
        result2.task(":${SYNC_TASK_NAME}").outcome == UP_TO_DATE
    }

    void writeSingleProject() {
        buildFile.text = """
        plugins {
            id 'org.ysb33r.ivypot'
        }
        
        repositories {
            mavenCentral()
        }
        
        configurations {
          ivy1 
        }
        
        dependencies {
            ivy1 'commons-io:commons-io:2.17.0'
        }
        
        ivypot {
            binaryRepositories {
                source 'nodejs', {
                    rootUri = 'https://nodejs.org/dist/'
                    artifactPattern = 'v[revision]/[module]-v[revision]-[classifier].[ext]'
                }
            }

            cacheBinaries {
                add 'nodejs:node:7.10.0:linux-x64@tar.xz'
            }
        }
        """.stripIndent()

        settingsFile.text = ''
    }

    void writeMultiProject() {

        buildFile.text = '''
        plugins {
            id 'org.ysb33r.ivypot'
        }
        
        repositories {
            mavenCentral()
        }
        
        ivypot {
            projectsWithDependencies { name -> true }
            binaryRepositories {
                source 'nodejs', {
                    rootUri = 'https://nodejs.org/dist/'
                    artifactPattern = 'v[revision]/[module]-v[revision]-[classifier].[ext]'
                }
            }
        }
        '''.stripIndent()

        final subdir = new File(projectDir, subProject)
        subdir.mkdirs()
        new File(subdir, 'build.gradle').text = '''
        plugins {
            id 'org.ysb33r.ivypot'
        }
        
        repositories {
            mavenCentral()
        }
        
        configurations {
          ivy1 
        }
        
        dependencies {
            ivy1 'commons-io:commons-io:2.17.0'
        }
        
        ivypot {
            cacheBinaries {
                add 'nodejs:node:7.10.0:linux-x64@tar.xz'
            }
        }
        
        tasks.register('printRepoLocation') {
          final rd =  project.extensions.ivypot.repoDetails.root
          inputs.dir(rd)
          doLast {
            println inputs.files.files.size()
          }
        }
        '''.stripIndent()

        settingsFile.text = """
        include '${subProject}'
        """.stripIndent()
    }
}