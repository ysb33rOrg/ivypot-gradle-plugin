/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.gradle

import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.ivypot.gradle.errors.BadBinaryException
import org.ysb33r.ivypot.gradle.extensions.OfflineRepositorySingleProjectExtension
import org.ysb33r.ivypot.gradle.testfixtures.UnitTestSpecification

import static org.ysb33r.ivypot.gradle.testfixtures.IntegrationSpecification.getEscapedPathString

class OfflineRepositorySingleProjectExtensionSpec extends UnitTestSpecification {

    OfflineRepositorySingleProjectExtension ivypot

    void setup() {
        ivypot = project.extensions.create(
            OfflineRepositoryPlugin.EXTENSION_NAME,
            OfflineRepositorySingleProjectExtension,
            project
        )
    }

    void 'Can configure repo'() {
        when:
        final rootDir = ivypot.repoDetails.root.get().name

        then:
        rootDir == '.offline-repo'

        when:
        ivypot.repoDetails {
            root = 'xyz'
        }

        then:
        ivypot.repoDetails.groovyRepository.get().absolutePath.endsWith(getEscapedPathString('xyz/repository.gradle'))
        ivypot.repoDetails.kotlinRepository.get().parentFile.parentFile == project.rootDir
    }

    void 'Can configure binary repositories'() {
        setup:
        final repoName = 'nodejs'
        final myRoot = 'https://nodejs.org/dist/'
        final myPattern = 'v[revision]/[module]-v[revision]-[classifier].[ext]'

        ivypot.binaryRepositories {
            source repoName, {
                rootUri = myRoot
                artifactPattern = myPattern
            }
        }

        when:
        final repos = ivypot.binaryRepositories.get()

        then:
        repos.size() == 1
    }

    void 'Can configure configurations'() {
        setup:
        final po = ProjectOperations.find(project)
        final declarable = 'declareAbc'
        final resolvable = 'resolvedAbc'
        po.configurations.createLocalRoleFocusedConfiguration(declarable, resolvable)
        project.repositories.mavenCentral()
        project.dependencies.add(declarable, 'org.ysb33r.gradle:grolifant5-unpacker-dmg:5.0.0-alpha.1')

        when:
        final deps = ivypot.configurations.dependencies.get()

        then:
        !deps.empty

        when:
        ivypot.configurations.filter { false }
        final deps2 = ivypot.configurations.dependencies.get()

        then:
        deps2.empty

        when:
        ivypot.configurations.filter { true }
        ivypot.configurations.named('abc')
        final deps3 = ivypot.configurations.dependencies.get()

        then:
        deps3.empty
    }

    void 'Can add binaries'() {
        when:
        ivypot.cacheBinaries.add('org.gradle:gradle-core:2.2@zoom')
        final artifact = ivypot.cacheBinaries.binaryDeclaration.get()

        then:
        artifact.size() == 1
        artifact.first().extension == 'zoom'
        artifact.first().classifier == null
    }

    void 'Binaries must contain an extension'() {
        when:
        ivypot.cacheBinaries.add('org.gradle:gradle-core:2.2')

        then:
        thrown(BadBinaryException)

        where:
        dep << [
            'org.gradle:gradle-core:2.2',
            'org.gradle:gradle-core:2.2:classified'
        ]
    }
}