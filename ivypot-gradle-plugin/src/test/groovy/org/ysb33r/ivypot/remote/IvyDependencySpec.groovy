/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.ivypot.remote

import spock.lang.Specification

class IvyDependencySpec extends Specification {

    void 'Can compare two IvyDependency instances'() {
        when:
        final xz = new IvyDependency(
            transitive: true,
            confFilter: '*',
            organisation: 'org.tukaani',
            module: 'xz',
            revision: '1.6',
            extension: null,
            classifier: null,
            typeFilter: '*'
        )
        final neko = new IvyDependency(
            transitive: true,
            confFilter: '*',
            organisation: 'net.sourceforge.nekohtml',
            module: 'nekohtml',
            revision: '1.9.14',
            extension: null,
            classifier: null,
            typeFilter: '*'
        )

        then:
        xz <=> neko

        when:
        Set<IvyDependency> deps = [] as SortedSet
        deps.addAll([neko])
        deps.addAll([xz])

        then:
        deps.size() == 2
    }
}